#!/usr/bin/env bash
# This .sh file will be sourced before starting your application.
# You can use it to put environment variables you want accessible
# to the server side of your app by using process.env.MY_VAR
#
# Example:
# export MONGO_URL="mongodb://localhost:27017/myapp-development"
# export ROOT_URL="http://localhost:3000"

export METEOR_SETTINGS=$(cat settings.json)
export PORT="6003"
export ROOT_URL="https://www.passprint.me"
export MOBILE_DDP_URL=$ROOT_URL
export MAIL_URL="smtp://postmaster%40mailgun.passprint.me:8a714a01cc8813de2620a8b2bd7cba33@smtp.mailgun.org:587"
export MONGO_URL="mongodb://passprintadmin:P%40%24%24Pr1ntP%40%24%24w0rd@ds139959.mlab.com:39959/passprint-prod"
