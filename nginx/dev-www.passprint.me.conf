####
# Add to /etc/nginx/nginx.conf if not present
##
# http {
#       map $http_upgrade $connection_upgrade {
#           default upgrade;
#           ''      close;
#       }
#
#       proxy_temp_path /tmp;
#       ssl_session_cache   shared:SSL:10m;
#       ssl_session_timeout 10m;
#   }
####

proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=one:8m max_size=3000m inactive=600m;

upstream upstream {
	server 127.0.0.1:3003;
	keepalive 64;
}

server {
	listen 80;
	server_name dev-www.passprint.me;
	rewrite ^ https://$http_host$request_uri? permanent;    # force redirect http to https
}

server {
	listen 443 ssl;
	server_name dev-www.passprint.me;
	keepalive_timeout         70;

	ssl_certificate         /etc/nginx/ssl/certs/wildcard.passprint.me.crt;
	ssl_certificate_key     /etc/nginx/ssl/keys/wildcard.passprint.me.key;
	ssl_protocols           TLSv1 TLSv1.1 TLSv1.2 SSLv3;
	ssl_ciphers 			RC4:HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers on;

	location / {
		proxy_pass         http://upstream/;
		proxy_set_header   X-Real-IP            $remote_addr;
		proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
		proxy_set_header   Host                   $http_host;
		proxy_set_header   X-NginX-Proxy    true;
		proxy_redirect off;

		# Handle caching
		proxy_cache one;
		proxy_cache_key sfs$request_uri$scheme;

		# Handle websocket requests
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
	}
}