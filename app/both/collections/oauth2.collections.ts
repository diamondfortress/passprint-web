import {IOAuthClient} from "../../both/models/oauth2-client.model";
import {IOAuth2Grant} from "../models/oauth2-grant.model";
export const OAuthClientsCollection = new Mongo.Collection<IOAuthClient>("OAuth2Clients");
export const AuthCodesCollection = new Mongo.Collection("OAuth2AuthCodes");
export const AccessTokensCollection = new Mongo.Collection("OAuth2AccessTokens");
export const RefreshTokensCollection = new Mongo.Collection("OAuth2RefreshTokens");
export const OAuth2GrantsCollection = new Mongo.Collection<IOAuth2Grant>("OAuth2Grants");

if (Meteor.isServer) {
    OAuthClientsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    OAuthClientsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });

    AuthCodesCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    AuthCodesCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });

    AccessTokensCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    AccessTokensCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });

    RefreshTokensCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    RefreshTokensCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });

    OAuth2GrantsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    OAuth2GrantsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}