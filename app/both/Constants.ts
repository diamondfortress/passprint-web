export class Constants {
    public static EMPTY_STRING = "";

    public static SESSION:any = {
        LANGUAGE: "language",
        LOADING: "isLoading",
        PLATFORM_READY: "platformReady",
        TRANSLATIONS_READY: "translationsReady",
        PATH: "path",
        URL_PARAMS: "urlParams",
        INCORRECT_PASSWORD: "incorrectPassword",
        FORGOT_PASSWORD: "forgotPassword",
        CREATE_ACCOUNT: "createAccount",
        RESET_PASSWORD: "resetPassword",
        REGISTERED_ERROR: "registeredError",
        NOT_REGISTERED_ERROR: "notRegisteredError",
        RESET_PASSWORD_ERROR: "resetPasswordError",
        RESET_PASSWORD_ERROR_MESSAGE: "resetPasswordErrorMessage",
        RESET_PASSWORD_TOKEN: "resetPasswordToken",
        WAS_PASSWORD_RESET: "wasPasswordReset",
        EMAIL: "email"
    };

    public static DEVICE:any = {
        IOS: "iOS",
        ANDROID: "Android"
    };

    public static STYLE:any = {
        IOS: "ios",
        MD: "md"
    };

    public static METEOR_ERRORS:any = {
        SIGN_IN: "sign-in",
        ACCOUNT_NOT_FOUND: "account-not-found",
        USER_NOT_FOUND: "User not found",
        LOGIN_WITH_OAUTH: "login-with-oauth",
        EMAIL_EXISTS: "email-exists",
        TOKEN_EXPIRED: "Token expired",
        VERIFY_EMAIL_LINK_EXPIRED: "Verify email link expired",
        TIMEDOUT: "ETIMEDOUT",
        REGISTRATION_COMPLETE: "registration-complete",
        REGISTRATION_INCOMPLETE: "registration-incomplete",
        OAUTH2_CLIENT_NOT_FOUND: "oauth2-client-error",
        AUTH_REQ_INVALID: "auth-req-invalid",
        ACCESS_DENIED: "access-denied"
    };
    
    public static ENVIRONMENT:any = {
        DEVELOPMENT: "DEVELOPMENT",
        PRODUCTION: "PRODUCTION"
    };

    public static ADD_IMAGE_PLACEHOLDER_URI:string = "/images/add_image_camera_photo.png";
    public static IMAGE_URI_PREFIX:string = "data:image/jpeg;base64,";

    public static NAVBAR_PASSPRINT_LOGO_URI:string = "/images/passprint_logo_white_196x44.png";
    public static CARD_PASSPRINT_LOGO_URI:string = "/images/passprint_logo_1112x249.png";
    public static PLACEHOLDER_OAUTH2_AUTHORIZE_IMAGE:string = "/images/onyx_lock.png";
        
    public static OAUTH2_AUTHORIZE_URL = Meteor.absoluteUrl() + "auth/oauth2/authorize?";

    public static PASSPRINT_PUSH_ENDPOINTS:any = {
        AUTHENTICATE: "/api/v1/notifications/authenticate"
    };
    
    public static OAUTH2_SCOPE:any = {
        EMAIL: "email",
        NAME: "name",
        PROFILE_PICTURE: "profile_picture",
        BIRTHDAY: "birthday",
        ADDRESS: "address"
    };
    
    public static COOKIES:any = {
        USER_ID: "userId",
        TOKEN: "token",
        ACCOUNTS: "accounts"
    };
    
    public static PASSPRINT_ROLES_GROUP:string = "roles.passprint.me";
    public static ROLES:any = {
        ADMIN: "administrator",
        USER: "user"
    };
    
    public static PUBLICATIONS:any = {
        AUTHENTICATION_REQUESTS: "AuthenticationRequests",
        RELYING_PARTIES: "RelyingParties",
        PASSPRINT_USERS: "PassPrintUsers",
        MY_PROJECTS: "MyProjects",
        PROJECT_CREDENTIALS: "ProjectCredentials",
        PROJECT_AUTHENTICATIONS: "ProjectAuthentications",
        PROJECT_CLIENT_GRANTS: "ProjectClientGrants",
        PROJECT_BILLING_AGGREGATE: "ProjectBillingAggregate"
    };
    
    public static PATHS:any = {
        CREATE_ACCOUNT: "/create-account"
    };
}