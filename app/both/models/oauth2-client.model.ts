import * as moment from 'moment';
import {Constants} from "../Constants";

export interface IOAuthClient {
    _id?:string,
    userId?:string,
    projectId?:string,
    clientId?:string,
    clientSecret?:string,
    created?:string,
    clientName?:string,
    redirectUri?:Array<string>,
    origins?:Array<string>,
    active?:boolean
}

export class OAuthClient implements IOAuthClient {
    public _id:string;
    public userId:string;
    public projectId:string;
    public clientId:string;
    public clientSecret:string;
    public clientName:string = Constants.EMPTY_STRING;
    public created:string = moment().toISOString();
    public redirectUri:Array<string> = [];
    public origins:Array<string> = [];
    public active:boolean = true;

    constructor(oauthClient:IOAuthClient) {
        this.clientId = Random.hexString(32) + ".oauth2.apps.passprint.me";
        this.clientSecret = Random.secret();
        if (oauthClient) {
            this._id = oauthClient._id;
            this.userId = oauthClient.userId;
            this.projectId = oauthClient.projectId;
            if (oauthClient.hasOwnProperty("clientId")) {
                this.clientId = oauthClient.clientId;
            }
            if (oauthClient.hasOwnProperty("clientSecret")) {
                this.clientSecret = oauthClient.clientSecret;
            }
            if (oauthClient.hasOwnProperty("clientName")) {
                this.clientName = oauthClient.clientName;
            }
            if (oauthClient.hasOwnProperty("created")) {
                this.created = oauthClient.created;
            }
            if (oauthClient.hasOwnProperty("redirectUri")) {
                let redirectUri:Array<string> = [];
                oauthClient.redirectUri.forEach((uri:string) => {
                    redirectUri.push(uri);
                });
                this.redirectUri = redirectUri;
            }
            if (oauthClient.hasOwnProperty("origins")) {
                let origins:Array<string> = [];
                oauthClient.origins.forEach((origin:string) => {
                    origins.push(origin);
                });
                this.origins = origins;
            }
            if (oauthClient.hasOwnProperty("active")) {
                this.active = oauthClient.active;
            }
        }
    }
}