import {Constants} from "../Constants";
export interface IAddress {
    streetAddress:string,
    locality:string,
    region:string,
    postalCode:string
}

export class Address {
    streetAddress:string = Constants.EMPTY_STRING;
    locality:string = Constants.EMPTY_STRING;
    region:string = Constants.EMPTY_STRING;
    postalCode:string = Constants.EMPTY_STRING;

    constructor(addressDetails?:IAddress) {
        if (addressDetails) {
            this.streetAddress = addressDetails.streetAddress;
            this.locality = addressDetails.locality;
            this.region = addressDetails.region;
            this.postalCode = addressDetails.postalCode;
        }
    }

    localityRegionPostal():string {
        return this.locality + ', ' + this.region + ' ' + this.postalCode;
    }
}