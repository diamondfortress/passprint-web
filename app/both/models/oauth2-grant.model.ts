import * as moment from "moment";
export interface IOAuth2Grant {
    _id?:string,
    userId?:string,
    clientId?:string,
    created?:Date
}

export class OAuth2Grant implements IOAuth2Grant {
    public _id:string;
    public userId:string;
    public clientId:string;
    public created:Date = new Date(moment().toISOString());
    
    constructor(grant?:IOAuth2Grant) {
        if (grant) {
            this._id = grant._id;
            this.userId = grant.userId;
            this.clientId = grant.clientId;
            if (grant.hasOwnProperty("created")) {
                this.created = grant.created;
            }
        }
    }
}