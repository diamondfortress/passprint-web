import {Constants} from "../Constants";
import * as moment from 'moment';
export interface IProject {
    _id?:string,
    userId?:string,
    displayName?:string,
    logo?:string,
    homepageUrl?:string,
    privacyPolicyUrl?:string,
    tosUrl?:string,
    created?:string
}

export class Project implements IProject {
    public _id:string;
    public userId:string;
    public displayName:string = Constants.EMPTY_STRING;
    public logo:string = Constants.EMPTY_STRING;
    public homepageUrl:string = Constants.EMPTY_STRING;
    public privacyPolicyUrl:string = Constants.EMPTY_STRING;
    public tosUrl:string = Constants.EMPTY_STRING;
    public created:string = moment().toISOString();
    
    constructor(projectJson?:IProject) {
        if (projectJson) {
            this._id = projectJson._id;
            this.userId = projectJson.userId;
            this.logo = projectJson.logo;
            if (projectJson.hasOwnProperty("displayName")) {
                this.displayName = projectJson.displayName;
            }
            if (projectJson.hasOwnProperty("homepageUrl")) {
                this.homepageUrl = projectJson.homepageUrl;
            }
            if (projectJson.hasOwnProperty("privacyPolicyUrl")) {
                this.privacyPolicyUrl = projectJson.privacyPolicyUrl;
            }
            if (projectJson.hasOwnProperty("tosUrl")) {
                this.tosUrl = projectJson.tosUrl;
            }
            if (projectJson.hasOwnProperty("created")) {
                this.created = projectJson.created;
            }
        }
    }
}