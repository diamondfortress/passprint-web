export interface AuthenticationRequest {
    _id?:string,
    userId:string,
    clientId:string,
    sentAt?:string,
    isVerified?:boolean,
    loginToken?:string,
    isRejected?:boolean
}