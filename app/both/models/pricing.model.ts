export interface IPricing {
    _id?:string,
    increment?:number,
    levels?:Array<IPriceTier>,
    total?:number
}

export interface IPriceTier {
    users?:number,
    rate?:number,
    total?:number
}