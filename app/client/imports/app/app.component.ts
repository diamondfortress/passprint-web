import {Component, OnInit, NgZone, ViewChild} from "@angular/core";
import {MeteorComponent} from "angular2-meteor";
import {Platform, LoadingController, Loading} from "ionic-angular";
import {Constants} from "../../../both/Constants";
import {TranslateService} from "ng2-translate";
import {HomePage} from "./pages/home/home";
import {LoginPage} from "./pages/account/login/login";
import {CreateAccountPage} from "./pages/account/create-account/create-account";
import {VerifyEmailPage} from "./pages/account/verify-email/verify-email";
import {AccountMenuPage} from "./pages/account/account-menu/account-menu";
import {AboutPage} from "./pages/about/about";
import template from "./app.component.html";
import {AdminPage} from "./pages/admin/admin";
import {DeveloperPage} from "./pages/developer/developer";
import {RequestHelper} from "../../../both/RequestHelper";

declare var Roles;
//declare var Cookie;

@Component({
    selector: "ion-app",
    template: template
})
export class AppComponent extends MeteorComponent implements OnInit {
    @ViewChild('leftMenu') leftMenu:any;
    @ViewChild('content') nav:any;

    public rootPage:any = HomePage;
    public appName:string;
    public user:Meteor.User;
    private isLoading:boolean = false;
    private loading:Loading;
    public pages:Array<INavigationMenuPage>;
    public userPages:Array<INavigationMenuPage>;
    public noUserPages:Array<INavigationMenuPage>;
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;

    constructor(public platform:Platform,
                public loadingCtrl:LoadingController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.parseUrl();
        this.initializeApp();
        // set the nav menu title to the application name from settings.json
        this.appName = Meteor.settings.public["appName"];

        // set our app's pages
        // title references a key in the language JSON to be translated by the translate pipe in the HTML
        this.noUserPages = [{
            icon: "log-in",
            title: "page-login.title",
            component: LoginPage
        }];
        this.pages = [{
            icon: "home",
            title: "page-home.title",
            component: HomePage,
            rootPage: true
        }, {
            icon: "create",
            title: "page-create-account.title",
            component: CreateAccountPage
        }, {
            icon: "paper-plane",
            title: "page-verify-email.title",
            component: VerifyEmailPage
        }, {
            icon: "information-circle",
            title: "page-about.title",
            component: AboutPage
        }];

        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
            console.log("user: ", this.user);
            this.setUserPages();

            //Update the cookie whenever they log in or out
            // Cookie.set(Constants.COOKIES.USER_ID, Meteor.userId());
            // Cookie.set(Constants.COOKIES.TOKEN, localStorage.getItem("Meteor.loginToken"));
        }));

        this.autorun(() => this.zone.run(() => {
            if (Session.get(Constants.SESSION.PLATFORM_READY)) {
                this.platformReady();

                // Reset PLATFORM_READY flag so styles will be applied correctly with hotcode push
                Session.set(Constants.SESSION.PLATFORM_READY, false);
            }
        }));

        this.translate.onLangChange.subscribe(() => {
            Session.set(Constants.SESSION.TRANSLATIONS_READY, true);
        });

        // Global loading dialog
        this.autorun(() => {
            // Use Session.set(Constants.SESSION.LOADING, true); to trigger loading dialog
            if (Session.get(Constants.SESSION.LOADING)) {
                if (this.nav) {
                    // Delay to prevent showing if loaded quickly
                    // Meteor.setTimeout(() => {
                    if (Session.get(Constants.SESSION.LOADING)) {
                        this.loading = this.loadingCtrl.create({
                            spinner: 'crescent'
                            //content: 'Loggin in...'
                        });
                        this.loading.present();
                        this.isLoading = true;
                    }
                    // }, 500);
                }
            } else {
                if (this.isLoading && this.loading) {
                    this.loading.dismiss();
                }
            }
        });
    }

    private setUserPages():void {
        if (!this.user) {
            this.userPages = [];
        } else {
            let isPassPrintAdmin:boolean = false;
            if (Roles.userIsInRole(Meteor.userId(), [Constants.ROLES.ADMIN], Constants.PASSPRINT_ROLES_GROUP)) {
                isPassPrintAdmin = true;
            }
            this.userPages = [{
                icon: "fa-user-secret",
                isIconFA: true,
                title: 'menu.admin',
                component: AdminPage,
                hide: !isPassPrintAdmin,
                rootPage: true
            }, {
                icon: "construct",
                title: 'menu.developer',
                component: DeveloperPage,
                rootPage: true
            }];
        }
    }

    private initializeApp() {
        this.platform.ready().then(() => {
            Session.set(Constants.SESSION.PLATFORM_READY, true);
        });
    }

    private platformReady():void {
        this.initializeTranslateServiceConfig();
        this.setStyle();
    }

    private initializeTranslateServiceConfig() {
        var userLang = navigator.language.split('-')[0];
        userLang = /(en|es)/gi.test(userLang) ? userLang : 'en';

        this.translate.setDefaultLang('en');
        let langPref = Session.get(Constants.SESSION.LANGUAGE);
        if (langPref) {
            userLang = langPref;
        }
        Session.set(Constants.SESSION.LANGUAGE, userLang);
        this.translate.use(userLang);
    }

    private setStyle():void {
        // Change value of the meta tag
        var links:any = document.getElementsByTagName("link");
        for (var i = 0; i < links.length; i++) {
            var link = links[i];
            if (link.getAttribute("rel").indexOf("style") != -1 && link.getAttribute("title")) {
                link.disabled = true;
                if (link.getAttribute("title") === this.getBodyStyle())
                    link.disabled = false;
            }
        }
    }

    private getBodyStyle():string {
        var bodyTag:any = document.getElementsByTagName("ion-app")[0];
        var bodyClass = bodyTag.className;
        var classArray = bodyClass.split(" ");
        var bodyStyle = classArray[0];
        return bodyStyle;
    }

    private openPage(page:INavigationMenuPage) {
        this.navigate(page);
    }

    private showAccountMenu():void {
        this.navigate({component: AccountMenuPage, rootPage: false});
    }

    private logout():void {
        this.user = null;
        Meteor.logout();
        this.navigate({component: HomePage, rootPage: true});
    }

    private navigate(page:INavigationMenuPage):void {
        // close the menu when clicking a link from the menu
        // getComponent selector is the component id attribute
        this.leftMenu.close().then(() => {
            if (page.component) {
                // navigate to the new page if it is not the current page
                let viewCtrl = this.nav.getActive();
                if (viewCtrl.component !== page.component) {
                    if (page.rootPage) {
                        this.nav.setRoot(page.component);
                    } else {
                        this.nav.push(page.component, page.navParams);
                    }
                }
            }
        });
    }

    private parseUrl():void {
        var path = RequestHelper.getPath(this.platform.url());
        var urlParams:any = RequestHelper.getUrlParams(this.platform.url());
        if (urlParams) {
            urlParams.string = RequestHelper.getUrlParamString(this.platform.url());
        }
        Session.set(Constants.SESSION.PATH, path);
        Session.set(Constants.SESSION.URL_PARAMS, urlParams);
        console.log("path: ", path);
        console.log("urlParams: ", urlParams);
    }
}

export interface INavigationMenuPage {
    icon?:string,
    isIconFA?:boolean,
    title?:string,
    component:any,
    rootPage?:boolean,
    navParams?:any,
    hide?:boolean
}
