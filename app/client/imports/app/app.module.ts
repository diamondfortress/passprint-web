import {NgModule, ErrorHandler} from "@angular/core";
import {CommonModule} from '@angular/common';
import {ResponsiveModule} from 'ng2-responsive';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from '@angular/http';
import {TranslateModule} from 'ng2-translate';
import {AppComponent} from "./app.component";
import {HomePage} from "./pages/home/home";
import {AboutPage} from "./pages/about/about";
import {LoginFormComponent} from "./components/login-form/login-form";
import {OauthProviderComponent} from "./components/oauth/oauth-provider";
import {CreateAccountCardComponent} from "./components/create-account-card/create-account-card";
import {VerifyEmailCardComponent} from "./components/verify-email-card/verify-email-card";
import {AccountMenuPage} from "./pages/account/account-menu/account-menu";
import {EditProfilePage} from "./pages/account/account-menu/edit-profile/edit-profile";
import {AddImageComponent} from "./components/add-image/add-image";
import {LanguageSelectComponent} from "./components/language-select/language-select";
import {LoginPage} from "./pages/account/login/login";
import {CreateAccountPage} from "./pages/account/create-account/create-account";
import {VerifyEmailPage} from "./pages/account/verify-email/verify-email";
import {AdminPage} from "./pages/admin/admin";
import {UserManagementPage} from "./pages/admin/user-management/user-management";
import {PassPrintUserItemComponent} from "./components/passprint-user-item/passprint-user-item";
import {PaginationComponent} from "./components/pagination-component/pagination-component";
import {PassPrintUserItemPopoverComponent} from "./components/passprint-user-item/passprint-user-item-popover/passprint-user-item-popover";
import {DeveloperPage} from "./pages/developer/developer";
import {BillingAccountPage} from "./pages/developer/billing-account/billing-account";
import {AddressFormComponent} from "./components/address-form/address-form";
import {ProjectsPage} from "./pages/developer/projects/projects";
import {ProjectInfoPage} from "./pages/developer/projects/project-info/project-info";
import {ProjectMenuPage} from "./pages/developer/projects/project-menu/project-menu";
import {ProjectCredentialsPage} from "./pages/developer/projects/project-credentials/project-credentials";
import {EditOAuthClientPage} from "./pages/developer/projects/project-credentials/edit-oauth-client/edit-oauth-client";
import {AuthorizedOriginModalPage} from "./pages/developer/projects/project-credentials/edit-oauth-client/authorized-origin-modal/authorized-origin-modal";
import {AuthorizedRedirectModalPage} from "./pages/developer/projects/project-credentials/edit-oauth-client/authorized-redirect-modal/authorized-redirect-modal";
import {ProjectDashboardPage} from "./pages/developer/projects/project-dashboard/project-dashboard";
import {MomentPipe} from "./pipes/moment.pipe";

@NgModule({
    // Components(Pages), Pipes, Directive
    declarations: [
        AppComponent,
        HomePage,
        LanguageSelectComponent,
        AboutPage,
        LoginPage,
        LoginFormComponent,
        CreateAccountPage,
        CreateAccountCardComponent,
        VerifyEmailPage,
        VerifyEmailCardComponent,
        OauthProviderComponent,
        AccountMenuPage,
        EditProfilePage,
        AddImageComponent,
        AdminPage,
        UserManagementPage,
        PassPrintUserItemComponent,
        PassPrintUserItemPopoverComponent,
        PaginationComponent,
        DeveloperPage,
        BillingAccountPage,
        AddressFormComponent,
        ProjectsPage,
        ProjectInfoPage,
        ProjectMenuPage,
        ProjectCredentialsPage,
        EditOAuthClientPage,
        AuthorizedOriginModalPage,
        AuthorizedRedirectModalPage,
        ProjectDashboardPage,
        MomentPipe
    ],
    // Pages
    entryComponents: [
        AppComponent,
        HomePage,
        LoginPage,
        CreateAccountPage,
        VerifyEmailPage,
        AboutPage,
        AccountMenuPage,
        EditProfilePage,
        AdminPage,
        UserManagementPage,
        PassPrintUserItemPopoverComponent,
        DeveloperPage,
        BillingAccountPage,
        ProjectsPage,
        ProjectInfoPage,
        ProjectMenuPage,
        ProjectCredentialsPage,
        EditOAuthClientPage,
        AuthorizedOriginModalPage,
        AuthorizedRedirectModalPage,
        ProjectDashboardPage
    ],
    // Providers
    providers: [
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        }
    ],
    // Modules
    imports: [
        BrowserModule,
        HttpModule,
        CommonModule,
        ResponsiveModule,
        TranslateModule.forRoot(),
        IonicModule.forRoot(AppComponent, {
            //// http://ionicframework.com/docs/v2/api/config/Config/
            //mode: Constants.STYLE.MD,
            //pageTransition: Constants.STYLE.IOS,
            //swipeBackEnabled: false,
            //tabbarPlacement: 'top'
        }),
    ],
    // Main Component
    bootstrap: [IonicApp]
})
export class AppModule {
    constructor() {

    }
}
