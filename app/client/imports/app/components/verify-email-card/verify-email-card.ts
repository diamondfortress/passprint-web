import {Component, OnInit, NgZone} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular';
import {FormBuilder, Validators, AbstractControl, FormGroup} from '@angular/forms';
import {MeteorComponent} from 'angular2-meteor';
import {HomePage} from '../../pages/home/home';
import {Constants} from "../../../../../both/Constants";
import {FormValidator} from "../../utils/FormValidator";
import {ToastMessenger} from "../../utils/ToastMessenger";
import {TranslateService} from 'ng2-translate';
import {CreateAccountPage} from "../../pages/account/create-account/create-account";

import template from "./verify-email-card.html";
@Component({
    selector: 'verify-email-card',
    template: template
})
export class VerifyEmailCardComponent extends MeteorComponent implements OnInit {
    public cardPassPrintLogoUri:string = Constants.CARD_PASSPRINT_LOGO_URI;
    public verifyEmailForm:FormGroup;
    public formControl:{
        email:AbstractControl
    };
    public formInputs: {
        email:string
    };

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public fb:FormBuilder,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.formInputs = {
            email: Constants.EMPTY_STRING
        };
        this.verifyEmailForm = this.fb.group({
            'email': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.validEmail,
                FormValidator.registered
            ])]
        });

        this.formControl = {
            email: this.verifyEmailForm.controls['email']
        };

        this.autorun(() => this.zone.run(() => {
            Session.get(Constants.SESSION.REGISTERED_ERROR);
            this.formInputs.email = Session.get(Constants.SESSION.EMAIL) || null;
        }));
    }

    public onSubmit():void {
        Session.set(Constants.SESSION.LOADING, true);
        var self = this;
        if (this.verifyEmailForm.valid) {
            Session.set(Constants.SESSION.EMAIL, this.formInputs.email);
            
            Meteor.call('sendVerificationEmail', {email: this.formInputs.email.toLowerCase()}, (error, result) => {
                Session.set(Constants.SESSION.LOADING, false);
                if (error) {
                    console.log("Error sending verification email: " + JSON.stringify(error));
                    if (error.error === Constants.METEOR_ERRORS.REGISTRATION_COMPLETE) {
                        new ToastMessenger().toast({
                            type: "error",
                            message: error.reason
                        });
                    }
                } else {
                    let alert = this.alertCtrl.create({
                        title: self.translate.instant("verify-email-card.alerts.verifyEmailSent.title"),
                        message: self.translate.instant("verify-email-card.alerts.verifyEmailSent.message"),
                        buttons: [
                            {
                                text: self.translate.instant("general.ok"),
                                handler: () => {
                                }
                            }
                        ],
                        enableBackdropDismiss: false
                    });
                    alert.present();
                }
            });
        }
    }

    public showCreateAccountCard():void {
        Session.set(Constants.SESSION.EMAIL, this.formInputs.email);
        this.nav.push(CreateAccountPage);
    }
}