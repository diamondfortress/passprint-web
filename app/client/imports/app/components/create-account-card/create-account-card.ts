import {Component, OnInit} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular';
import {FormBuilder, Validators, AbstractControl, FormGroup} from '@angular/forms';
import {MeteorComponent} from 'angular2-meteor';
import {HomePage} from '../../pages/home/home';
import {Constants} from "../../../../../both/Constants";
import {FormValidator} from "../../utils/FormValidator";
import {ToastMessenger} from "../../utils/ToastMessenger";
import {TranslateService} from "ng2-translate";
import {LoginPage} from "../../pages/account/login/login";
import {VerifyEmailPage} from "../../pages/account/verify-email/verify-email";

import template from "./create-account-card.html";
@Component({
    selector: 'create-account-card',
    template: template
})
export class CreateAccountCardComponent extends MeteorComponent implements OnInit {
    public cardPassPrintLogoUri:string = Constants.CARD_PASSPRINT_LOGO_URI;
    public createAccountForm:FormGroup;
    public formControl:{
        givenName:AbstractControl,
        familyName:AbstractControl,
        email:AbstractControl,
        confirmEmail:AbstractControl
    };

    public user:{
        email:string,
        profile:{
            name:{
                given:string,
                family:string,
                display:string
            }
        }
    };

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public fb:FormBuilder,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        /* Setup form controls */
        this.createAccountForm = this.fb.group({
            'givenName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
            ])],
            'familyName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
            ])],
            'email': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.validEmail,
                FormValidator.notRegistered
            ])],
            'confirmEmail': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.validEmail
            ])]
        }, {
            validator: Validators.compose([
                FormValidator.matchingFields('mismatchedEmails', 'email', 'confirmEmail')
            ])
        });

        this.formControl = {
            givenName: this.createAccountForm.controls['givenName'],
            familyName: this.createAccountForm.controls['familyName'],
            email: this.createAccountForm.controls['email'],
            confirmEmail: this.createAccountForm.controls['confirmEmail'],
        };
        /* End for control */

        this.user = {
            email: Constants.EMPTY_STRING,
            profile: {
                name: {
                    given: Constants.EMPTY_STRING,
                    family: Constants.EMPTY_STRING,
                    display: Constants.EMPTY_STRING
                }
            }
        };

        this.autorun(() => {
            this.user.email = Session.get(Constants.SESSION.EMAIL) || null;
            Session.get(Constants.SESSION.NOT_REGISTERED_ERROR);
        });
    }

    private createAccount():void {
        if (this.createAccountForm.valid) {
            var self = this;
            Session.set(Constants.SESSION.EMAIL, self.user.email);

            Meteor.call('createAccount', self.user, (error, result) => {
                if (error) {
                    console.log("Error creating user: " + JSON.stringify(error));
                    var toastMessage = Constants.EMPTY_STRING;
                    if (error.reason) {
                        if (error.error === Constants.METEOR_ERRORS.EMAIL_EXISTS) {
                            Session.set(Constants.SESSION.NOT_REGISTERED_ERROR, true);
                            self.formControl.email.updateValueAndValidity(true);
                            error.reason = self.translate.instant(
                                "create-account-card.errors.alreadyRegistered");
                        }
                        toastMessage = error.reason;
                    } else {
                        toastMessage = error.message;
                    }
                    new ToastMessenger().toast({
                        type: "error",
                        message: toastMessage,
                        title: self.translate.instant(
                            "create-account-card.errors.createAccount")
                    });
                } else {
                    console.log("Create user result: ", result);
                    let alert = this.alertCtrl.create({
                        title: self.translate.instant("verify-email-card.alerts.verifyEmailSent.title"),
                        message: self.translate.instant("verify-email-card.alerts.verifyEmailSent.message"),
                        buttons: [
                            {
                                text: self.translate.instant("general.ok"),
                                handler: () => {
                                    self.nav.setRoot(HomePage);
                                }
                            }
                        ],
                        enableBackdropDismiss: false
                    });
                    alert.present();
                }
            });
        }
    }

    private showVerifyEmailCard():void {
        Session.set(Constants.SESSION.EMAIL, this.user.email);
        this.nav.push(VerifyEmailPage);
    }

    private showSignInCard():void {
        Session.set(Constants.SESSION.EMAIL, this.user.email);
        this.nav.push(LoginPage);
    }
}