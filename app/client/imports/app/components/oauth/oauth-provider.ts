import {Component, Input} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular';
import {HomePage} from '../../pages/home/home';
import {TranslateService} from 'ng2-translate';
import {ToastMessenger} from "../../utils/ToastMessenger";
import {Constants} from "../../../../../both/Constants";

declare var Meteor; // Because loginWithPassPrint is not in the Type definition for Meteor

import template from "./oauth-provider.html";
@Component({
    selector: 'oauth-provider',
    template: template
})
export class OauthProviderComponent {
    @Input() oauthProvider;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public translate:TranslateService) {
    }

    private loginWithProvider(provider):void {
        console.log("loginWith" + provider);
        var self = this;
        switch (provider) {
            case "Google":
                Meteor.loginWithGoogle({
                    requestPermissions: [
                        'https://www.googleapis.com/auth/userinfo.profile',
                        'https://www.googleapis.com/auth/userinfo.email'
                    ],
                }, function (error) {
                    if (error) {
                        console.log("Error: " + JSON.stringify(error));
                        var errorMessage = error.message;
                        if (error.error === Constants.METEOR_ERRORS.LOGIN_WITH_OAUTH) {
                            errorMessage = error.reason;
                        } else if (error.error === Constants.METEOR_ERRORS.EMAIL_EXISTS) {
                            errorMessage = self.translate.instant(
                                "create-account-card.errors.alreadyRegistered");
                        } else if (error.errorType === "Accounts.LoginCancelledError") {
                            errorMessage = null;
                        }
                        if (errorMessage) {
                            console.log("Error signing in with Google: " + errorMessage);
                            new ToastMessenger().toast({
                                type: "error",
                                message: errorMessage,
                                title: self.translate.instant("login-card.errors.signIn")
                            });
                        }
                    } else {
                        console.log("Successfully signed in with Google");
                        self.showVerifyEmailAlert();
                    }
                });
                break;
            case "Facebook":
                Meteor.loginWithFacebook({
                    requestPermissions: ['email', 'user_birthday', 'user_location'],
                }, function (error) {
                    if (error) {
                        console.log("Error: " + JSON.stringify(error));
                        var errorMessage = error.message;
                        if (error.error === Constants.METEOR_ERRORS.LOGIN_WITH_OAUTH) {
                            errorMessage = error.reason;
                        } else if (error.error === Constants.METEOR_ERRORS.EMAIL_EXISTS) {
                            errorMessage = self.translate.instant(
                                "create-account-card.errors.alreadyRegistered");
                        } else if (error.errorType === "Accounts.LoginCancelledError") {
                            errorMessage = null;
                        }
                        if (errorMessage) {
                            console.log("Error signing in with Facebook: " + errorMessage);
                            new ToastMessenger().toast({
                                type: "error",
                                message: errorMessage,
                                title: self.translate.instant("login-card.errors.signIn")
                            });
                        }
                    } else {
                        console.log("Successfully signed in with Facebook");
                        self.showVerifyEmailAlert();
                    }
                });
                break;
            default:
                console.log("Provider not listed");
        }
    }

    private showVerifyEmailAlert():void {
        var self = this;
        let alert = this.alertCtrl.create({
            title: self.translate.instant("verify-email-card.alerts.verifyEmailSent.title"),
            message: self.translate.instant("verify-email-card.alerts.verifyEmailSent.message"),
            buttons: [
                {
                    text: self.translate.instant("general.ok"),
                    handler: () => {
                        self.nav.setRoot(HomePage);
                    }
                }
            ],
            enableBackdropDismiss: false
        });
        alert.present();
    }
}