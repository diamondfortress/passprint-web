import {Component, OnInit, NgZone, Output, EventEmitter} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular';
import {FormBuilder, Validators, AbstractControl, FormGroup} from '@angular/forms';
import {MeteorComponent} from 'angular2-meteor';
import {HomePage} from '../../pages/home/home';
import {Constants} from "../../../../../both/Constants";
import {FormValidator} from "../../utils/FormValidator";
import {ToastMessenger} from "../../utils/ToastMessenger";
import {TranslateService} from 'ng2-translate';
import {CreateAccountPage} from "../../pages/account/create-account/create-account";

declare var Meteor; //Property 'loginWithToken' does not exist on type 'typeof Meteor'.

import template from "./login-form.html";
@Component({
    selector: 'login-form',
    template: template
})
export class LoginFormComponent extends MeteorComponent implements OnInit {
    @Output() onAccountFound:EventEmitter<{user:Meteor.User}> = new EventEmitter();
    public user:Meteor.User;
    public loginForm:FormGroup;
    public formControl:{
        email:AbstractControl
    };
    public formInputs:{
        email:string
    };

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public fb:FormBuilder,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.formInputs = {
            email: null
        };
        this.loginForm = this.fb.group({
            'email': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.validEmail,
                FormValidator.registered
            ])]
        });

        this.formControl = {
            email: this.loginForm.controls['email']
        };

        this.autorun(() => this.zone.run(() => {
            Session.get(Constants.SESSION.REGISTERED_ERROR);
            this.formInputs.email = Session.get(Constants.SESSION.EMAIL) || null;
        }));
    }

    public onSubmit():void {
        var self = this;
        if (this.loginForm.valid) {
            var email:string = this.formInputs.email;
            Session.set(Constants.SESSION.EMAIL, email);
            Meteor.call('getAccountForLogin', {email: email.toLowerCase()}, (error, result) => {
                if (error) {
                    console.log("getAccountForLogin() Error: ", error);
                    var toastMessage = null;
                    if (error.reason) {
                        if (error.error === Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND ||
                            error.error === Constants.METEOR_ERRORS.REGISTRATION_INCOMPLETE) {
                            Session.set(Constants.SESSION.REGISTERED_ERROR, true);
                            self.formControl.email.updateValueAndValidity(true);
                        } else {
                            toastMessage = error.reason;
                        }
                    } else {
                        toastMessage = error.message;
                    }
                    if (toastMessage) {
                        new ToastMessenger().toast({
                            type: "error",
                            message: toastMessage,
                            title: self.translate.instant("login-card.errors.account")
                        });
                    }
                } else {
                    console.log("account: ", result);
                    self.onAccountFound.emit({user: result});
                }
            });
        }
    }
}

export interface ILoginFormComponent {
    onAccountFound($event:{user:Meteor.User}):void;
}