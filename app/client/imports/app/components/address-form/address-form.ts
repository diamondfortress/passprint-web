import {Component, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder, Validators, AbstractControl, FormGroup} from '@angular/forms';
import {Address} from "../../../../../both/models/address.model";
import template from './address-form.html';
import {FormValidator} from "../../utils/FormValidator";
@Component({
    selector: 'address-form',
    template: template
})
export class AddressFormComponent {
    @Input() address:Address;
    @Output() onAddressFormValueChanged:EventEmitter<{valid:boolean}> = new EventEmitter();
    private addressForm:FormGroup;
    private addressControl:{
        streetAddress:AbstractControl,
        locality:AbstractControl,
        region:AbstractControl,
        postalCode:AbstractControl
    };

    constructor(private fb:FormBuilder) {
    }

    ngOnInit() {
        this.addressForm = this.fb.group({
            'streetAddress': ['', Validators.compose([Validators.required])],
            'locality': ['', Validators.compose([Validators.required])],
            'region': ['', Validators.compose([Validators.required, FormValidator.validAddressRegion])],
            'postalCode': ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(5)])]
        });
        this.addressControl = {
            streetAddress: this.addressForm.controls['streetAddress'],
            locality: this.addressForm.controls['locality'],
            region: this.addressForm.controls['region'],
            postalCode: this.addressForm.controls['postalCode']
        };

        // Report to parent if this form is valid
        this.addressForm.valueChanges.subscribe(data => {
            this.onAddressFormValueChanged.emit({valid: this.addressForm.valid});
        });
    }
}

export interface IAddressFormComponent {
    onAddressFormValueChanged($event:{valid:boolean}):void;
}