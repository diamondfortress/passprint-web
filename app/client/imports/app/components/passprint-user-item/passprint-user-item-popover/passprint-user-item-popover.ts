import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, ViewController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../../both/Constants";
import template from "./passprint-user-item-popover.html";
declare var Roles;
@Component({
    selector: 'page-passprint-user-item-popover',
    template: template
})
export class PassPrintUserItemPopoverComponent extends MeteorComponent implements OnInit {
    private user:Meteor.User;
    private isAdmin:boolean;
    private currentUserSelected:boolean;

    constructor(public nav:NavController,
                public params:NavParams,
                public viewCtrl:ViewController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.user = this.params.data.user;
        console.log("selected user: ", this.user);
        this.isAdmin = Roles.userIsInRole(
            this.user._id,
            [Constants.ROLES.ADMIN],
            Constants.PASSPRINT_ROLES_GROUP
        );
        this.currentUserSelected = this.user._id === Meteor.userId();
    }

    private toggleAdminRole():void {
        this.viewCtrl.dismiss({toggleAdminRole:true});
    }

    private deleteUser():void {
        this.viewCtrl.dismiss({deleteUser:true});
    }
}