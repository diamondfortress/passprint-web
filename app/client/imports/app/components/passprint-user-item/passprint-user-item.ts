import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {AlertController, PopoverController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../both/Constants";
import {ToastMessenger} from "../../utils/ToastMessenger";
import template from "./passprint-user-item.html";
import {PassPrintUserItemPopoverComponent} from "./passprint-user-item-popover/passprint-user-item-popover";

declare var Roles;

@Component({
    selector: 'passprint-user-item',
    template: template
})
export class PassPrintUserItemComponent extends MeteorComponent implements OnInit {
    @Input() user:Meteor.User;
    @Input() index:number;
    @Output() onRemoveUser:EventEmitter<{user:Meteor.User}> = new EventEmitter();
    public isSelf:boolean;
    public isAdmin:boolean;

    constructor(public alertCtrl:AlertController,
                public popoverCtrl:PopoverController,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.isSelf = (this.user._id === Meteor.userId());
        this.isAdmin = Roles.userIsInRole(
            this.user._id,
            [Constants.ROLES.ADMIN],
            Constants.PASSPRINT_ROLES_GROUP
        );
    }

    private removeUser(user:Meteor.User):void {
        var self = this;
        if (!Roles.userIsInRole(Meteor.userId(), [Constants.ROLES.ADMIN], Constants.PASSPRINT_ROLES_GROUP)) {
            new ToastMessenger().toast({
                type: "error",
                message: self.translate.instant("general.errors.accessDenied")
            });
        }
        // else if (user._id === Meteor.userId()) {
        //     new ToastMessenger().toast({
        //         type: "error",
        //         message: self.translate.instant("passprint-user-item.errors.notRemoveSelf")
        //     });
        // } 
        else {
            let confirmDeleteAlert = self.alertCtrl.create({
                title: self.translate.instant("passprint-user-item.alerts.removeUser.title"),
                subTitle: user.profile.name.display + "\n" + user.emails[0].address,
                message: self.translate.instant("passprint-user-item.alerts.removeUser.message", {
                    value: user.profile.name.display
                }),
                buttons: [{
                    text: self.translate.instant("general.cancel"),
                    role: 'cancel',
                    handler: () => {
                    }
                }, {
                    text: self.translate.instant("general.delete"),
                    handler: () => {
                        self.onRemoveUser.emit({user: user});
                    }
                }],
                enableBackdropDismiss: false
            });
            confirmDeleteAlert.present();
        }
    }

    private presentPopover(event, user:Meteor.User):void {
        var self = this;
        let popover = self.popoverCtrl.create(PassPrintUserItemPopoverComponent, {user: user});
        popover.onDidDismiss((data:any) => {
            if (data) {
                if (data.toggleAdminRole) {
                    var userWasAdmin:boolean = Roles.userIsInRole(
                        user._id,
                        [Constants.ROLES.ADMIN],
                        Constants.PASSPRINT_ROLES_GROUP
                    );
                    Meteor.call("toggleAdminRole", {userId:user._id}, (error, result) => {
                        if (error) {
                            console.error("toggleAdminRole() Error: ", error);
                            let alert = self.alertCtrl.create({
                                title: self.translate.instant("passprint-user-item.errors.userManagementError"),
                                message: error.reason || error.message || error,
                                buttons: [{
                                    text: self.translate.instant("general.ok")
                                }]
                            });
                            alert.present();
                        } else {
                            console.log("result: ", result);
                            let message:string = self.translate.instant("passprint-user-item.madeAdmin");
                            if (userWasAdmin) {
                                message = self.translate.instant("passprint-user-item.removedAdmin");
                            }
                            new ToastMessenger().toast({
                                type: "success",
                                message: message
                            });
                        }
                    });
                } else if (data.deleteUser) {
                    self.removeUser(user);
                }
            }
        });
        popover.present({
            ev: event
        });
    }
}

export interface IPassPrintUserItemComponent {
    onRemoveUser($event:{user:Meteor.User}):void;
}