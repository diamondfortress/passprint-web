import {Component, OnInit, NgZone} from "@angular/core";
import {NavController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../both/Constants";
import template from "./admin.html";
import {OAuthClientsCollection} from "../../../../../both/collections/oauth2.collections";
import {UserManagementPage} from "./user-management/user-management";
@Component({
    selector: 'page-admin',
    template: template
})
export class AdminPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public user:Meteor.User;
    public userCount:number = 0;
    public relyingPartiesCount:number = 0;

    constructor(public nav:NavController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.subscribe(Constants.PUBLICATIONS.RELYING_PARTIES, () => {
                    console.log("<><><> Subscribed to " + Constants.PUBLICATIONS.RELYING_PARTIES);
                    this.autorun(() => this.zone.run(() => {
                        this.relyingPartiesCount = OAuthClientsCollection.find({}).count();
                    }));
                });
                this.subscribe(Constants.PUBLICATIONS.PASSPRINT_USERS, () => {
                    console.log("<><><> Subscribed to " + Constants.PUBLICATIONS.PASSPRINT_USERS);
                    this.autorun(() => this.zone.run(() => {
                        this.userCount = Meteor.users.find({}).count();
                    }));
                });
            }
        });
    }
    
    private manageUsers():void {
        this.nav.push(UserManagementPage);
    }
    
    private manageRelyingParties():void {
        
    }
}