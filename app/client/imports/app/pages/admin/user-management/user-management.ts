import {Component, OnInit, NgZone, ViewChild} from "@angular/core";
import {NavController, Content, AlertController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../../both/Constants";
import template from "./user-management.html";
import {Pagination} from "../../../components/pagination-component/pagination-component";
import {ToastMessenger} from "../../../utils/ToastMessenger";
declare var Roles;

@Component({
    selector: 'page-user-management',
    template: template
})
export class UserManagementPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public user:Meteor.User;
    private allUsers:Array<Meteor.User>;
    public filteredUsers:Array<Meteor.User> = [];
    public filteredAdmins:Array<Meteor.User> = [];
    public searchQuery:string = Constants.EMPTY_STRING;
    public showUsers:boolean = false;
    public showAdmins:boolean = false;

    // Pagination
    public paginationInfo:Pagination = {
        page: 1,
        pageSize: 50,
        rowCount: 0,
        pageCount: 0
    };
    @ViewChild(Content) content:Content;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.subscribe(Constants.PUBLICATIONS.PASSPRINT_USERS, () => {
                    console.log("<><><> Subscribed to " + Constants.PUBLICATIONS.PASSPRINT_USERS);
                    this.autorun(() => this.zone.run(() => {
                        this.allUsers = Meteor.users.find({}).fetch();
                        this.allUsers.sort(this.sortByFamilyName);
                        this.filterUsers();
                    }));
                });
            }
        });
    }

    private toggleShowAdmins():void {
        this.zone.run(() => {
            this.showAdmins = !this.showAdmins;
        });
    }

    private toggleShowUsers():void {
        this.zone.run(() => {
            this.showUsers = !this.showUsers;
        });
    }

    private getItems(event:any):void {
        this.searchQuery = event.target.value;
        this.filterUsers();
    }

    private filterUsers():void {
        // Reset items back to all of the items
        let filteredUsers:Array<any> = [];
        let filteredAdmins:Array<any> = [];
        this.allUsers.forEach((user:Meteor.User) => {
            if (Roles.userIsInRole(user._id, [Constants.ROLES.ADMIN], Constants.PASSPRINT_ROLES_GROUP)) {
                filteredAdmins.push(user);
            } else {
                filteredUsers.push(user);
            }
        });

        this.zone.run(() => {
            this.filteredUsers = filteredUsers;
            this.filteredAdmins = filteredAdmins;

            // if the value is an empty string don't filter the items
            if (this.searchQuery && this.searchQuery.trim() != Constants.EMPTY_STRING) {
                this.filteredUsers = this.filteredUsers.filter((user:any) => {
                    let queryString:string = user.profile.name.display + user.emails[0].address;
                    return (queryString.toLocaleLowerCase().indexOf(this.searchQuery.toLocaleLowerCase()) > -1);
                });
                this.filteredAdmins = this.filteredAdmins.filter((user:any) => {
                    let queryString:string = user.profile.name.display + user.emails[0].address;
                    return (queryString.toLocaleLowerCase().indexOf(this.searchQuery.toLocaleLowerCase()) > -1);
                });
            }

            let pageCount:number = Math.ceil(this.filteredUsers.length / this.paginationInfo.pageSize);
            if (this.paginationInfo.page > pageCount && pageCount > 0) {
                this.paginationInfo.page = pageCount;
            }
            this.paginationInfo.rowCount = this.filteredUsers.length;
            this.paginationInfo.pageCount = pageCount;

            this.content.resize();
        });
    }

    private onClearSearch(event:any):void {
        this.searchQuery = Constants.EMPTY_STRING;
        this.filterUsers();
    }

    public changePage(pageNum:number):void {
        this.zone.run(() => {
            this.paginationInfo.page = pageNum;
        });
    }

    get currentPageItemsMin() {
        let currentPageItemsMin:number = ((this.paginationInfo.page - 1) * this.paginationInfo.pageSize) + 1;
        return currentPageItemsMin;
    }

    get currentPageItemsMax() {
        let currentPageItemsMax:number = Math.min((this.paginationInfo.page) * this.paginationInfo.pageSize, this.maxItems);
        ;
        return currentPageItemsMax;
    }

    get maxItems() {
        return this.paginationInfo.rowCount;
    }

    private sortByFamilyName(a, b):number {
        if (a.profile.name.family.toLowerCase().trim() > b.profile.name.family.toLowerCase().trim()) {
            return 1;
        } else if (a.profile.name.family.toLowerCase().trim() < b.profile.name.family.toLowerCase().trim()) {
            return -1;
        } else {
            return 0;
        }
    }

    private deleteUser($event):void {
        var self = this;
        Meteor.call("removeUser", {userId: $event.user._id}, (error, result) => {
            if (error) {
                console.error("removeUser() Error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("passprint-user-item.errors.userManagementError"),
                    message: error.reason || error.message || error,
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }]
                });
                alert.present();
            } else {
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-user-management.toasts.deleteUser")
                });
            }
        });
    }
}