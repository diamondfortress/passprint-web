import {Component, OnInit, NgZone} from "@angular/core";
import {App, NavController, AlertController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../../both/Constants";
import {ILoginFormComponent} from "../../../components/login-form/login-form";
import {CreateAccountPage} from "../create-account/create-account";
import {AuthenticationRequest} from "../../../../../../both/models/authentication-request.model";
import {AuthenticationRequestsCollection} from "../../../../../../both/collections/authentication-requests.collection";
import {HomePage} from "../../home/home";
import template from "./login.html";
import {AccountMenuPage} from "../account-menu/account-menu";
declare var Cookie;
declare var Meteor;  // for loginWithToken()

@Component({
    selector: 'page-login',
    template: template
})
export class LoginPage extends MeteorComponent implements OnInit, ILoginFormComponent {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public cardPassPrintLogoUri:string = Constants.CARD_PASSPRINT_LOGO_URI;
    public user:Meteor.User;
    public imageUri:string = Constants.PLACEHOLDER_OAUTH2_AUTHORIZE_IMAGE;
    public isSigningIn:boolean = false;
    public isRejected:boolean = false;
    public accounts:Array<Meteor.User>;
    public removeAccounts:boolean = false;
    public showLoginForm:boolean = true;

    constructor(public app:App,
                public nav:NavController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.user = {};
        this.setUserImageUri();
        this.autorun(() => {
            // Wait for translations to be ready
            // in case component loads before the language is set
            // or the language is changed after the component has been rendered.
            // Since this is the home page, this component and any child components
            // will need to wait for translations to be ready.
            if (Session.get(Constants.SESSION.TRANSLATIONS_READY)) {
                this.translate.get('page-login.title').subscribe((translation:string) => {

                    // Set title of web page in browser
                    this.app.setTitle(translation);
                });
            }
        });
        this.refresh();
    }

    private refresh():void {
        var self = this;
        var accounts:Array<string> = JSON.parse(Cookie.get(Constants.COOKIES.ACCOUNTS));
        console.log("Accounts: ", accounts);

        if (accounts && accounts.length > 0) {
            this.showLoginForm = false;
            Meteor.call('getAccounts', accounts, (error, result) => {
                if (error) {
                    console.log("getAccounts() Error: ", error);
                } else {
                    self.zone.run(() => {
                        self.accounts = result;
                        self.setUserLoggedInStatus();
                    });
                }
            });
        } else {
            this.showLoginForm = true;
        }
    }

    private setUserLoggedInStatus():void {
        var user:Meteor.User = Meteor.user();

        if (this.accounts) {
            this.accounts.forEach((account:Meteor.User) => {
                var isLoggedIn:boolean = false;
                if (user) {
                    isLoggedIn = (account._id === user._id);
                }
                account.profile.isLoggedIn = isLoggedIn;
            });
        }
    }

    private setUserImageUri():void {
        if (this.user._id) {
            if (this.user.profile.picture) {
                this.imageUri = this.user.profile.picture;
            }
        }
    }

    onAccountFound($event:{user:Meteor.User}) {
        console.log("onAccountFound: ", $event);
        this.zone.run(() => {
            this.user = $event.user;
            this.showLoginForm = false;
            this.setUserImageUri();
        });
    }

    private goLogin(user:Meteor.User):void {
        this.zone.run(() => {
            this.user = user;
            this.setUserImageUri();
        });
    }

    private switchUser():void {
        this.user = {};
        this.refresh();
    }

    public showCreateAccountCard():void {
        this.nav.push(CreateAccountPage);
    }

    private addAccount():void {
        //this.nav.push(OAuth2LoginPage);
        this.showLoginForm = true;
    }

    private toggleRemoveAccounts():void {
        this.removeAccounts = !this.removeAccounts;
        if (this.accounts && this.accounts.length === 0) {
            this.showLoginForm = true;
        }
    }

    private removeAccount(user:Meteor.User):void {
        var index:number = this.accounts.indexOf(user);
        this.accounts.splice(index, 1);
        var accounts:Array<string> = JSON.parse(Cookie.get(Constants.COOKIES.ACCOUNTS));
        index = accounts.indexOf(user._id);
        accounts.splice(index, 1);
        Cookie.set(Constants.COOKIES.ACCOUNTS, JSON.stringify(accounts));
        if (user._id === Meteor.userId()) {
            Meteor.logout();
        }
    }

    private cancelAuthRequest():void {
        this.isSigningIn = false;
    }

    private signInWithPassPrint():void {
        var self = this;
        self.isSigningIn = true;
        self.isRejected = false;
        Meteor.call('loginWithPassPrint', {
            email: self.user.emails[0].address
        }, (error, result) => {
            if (error) {
                self.zone.run(() => {
                    self.isSigningIn = false;
                });
                console.log("Login Error: ", error);
                var errorMsg:string = error.message;
                if (!errorMsg) {
                    errorMsg = error;
                }
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("login-card.errors.signIn"),
                    message: errorMsg,
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }],
                });
                alert.present();
            } else {
                console.log("Login result: ", result);
                if (result.authReqId) {
                    self.subscribe(Constants.PUBLICATIONS.AUTHENTICATION_REQUESTS, result.authReqId, () => {
                        self.autorun(() => self.zone.run(() => {
                            var authReq:AuthenticationRequest = AuthenticationRequestsCollection.findOne({
                                _id: result.authReqId
                            });
                            console.log("authReq: ", authReq);
                            if (authReq) {
                                if (authReq.isRejected) {
                                    if (self.isSigningIn) {
                                        self.zone.run(() => {
                                            self.isSigningIn = false;
                                            self.isRejected = true;
                                        });
                                    }
                                } else if (authReq.isVerified && authReq.loginToken && self.isSigningIn) {
                                    self.loginWithToken(authReq.loginToken);
                                }
                            }
                        }), true);
                    });
                }
            }
        });
    }

    private loginWithToken(token:string):void {
        var self = this;
        Meteor.loginWithToken(token, (error) => {
            if (error) {
                self.zone.run(() => {
                    self.isSigningIn = false;
                });
                console.log("loginWithToken() Error: ", error);
                var errorMsg:string = error.message;
                if (!errorMsg) {
                    errorMsg = error;
                }
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("login-card.errors.signIn"),
                    message: errorMsg,
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }],
                });
                alert.present();
            } else {
                // Add user to Accounts cookie
                var userId:string = Meteor.userId();
                var accounts:Array<string> = JSON.parse(Cookie.get(Constants.COOKIES.ACCOUNTS)) || [];
                var addAccount:boolean = true;
                // Check if account has already been added
                accounts.forEach((account:string) => {
                    if (account === userId) {
                        addAccount = false;
                    }
                });

                if (addAccount && accounts) {
                    accounts.push(userId);
                }

                Cookie.set(Constants.COOKIES.ACCOUNTS, JSON.stringify(accounts));
                //self.nav.popToRoot();
                self.nav.setPages([{page: HomePage}, {page: AccountMenuPage}]);
            }
        });
    }
}