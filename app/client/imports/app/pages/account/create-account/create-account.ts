import {Component, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from 'ng2-translate';
import {Constants} from "../../../../../../both/Constants";

import template from './create-account.html';
@Component({
    selector: 'page-create-account',
    template: template
})
export class CreateAccountPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public oauthProviders:Array<{name: string, icon: string, color: string}>;

    constructor(public nav:NavController,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.oauthProviders = [
            {name: 'Google', icon:'logo-googleplus', color: "google"},
            {name: 'Facebook', icon:'logo-facebook', color: "facebook"}
        ];
    }
}