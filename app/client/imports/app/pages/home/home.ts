import {Component, OnInit, NgZone} from "@angular/core";
import {App, NavController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../both/Constants";
import template from "./home.html";
import {CreateAccountPage} from "../account/create-account/create-account";
@Component({
    selector: 'page-home',
    template: template
})
export class HomePage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public cardPassPrintLogoUri:string = Constants.CARD_PASSPRINT_LOGO_URI;
    public user:Meteor.User;

    constructor(public app:App,
                public nav:NavController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        // Use MeteorComponent autorun to respond to reactive session variables.
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();

            // Wait for translations to be ready
            // in case component loads before the language is set
            // or the language is changed after the component has been rendered.
            // Since this is the home page, this component and any child components
            // will need to wait for translations to be ready.
            if (Session.get(Constants.SESSION.TRANSLATIONS_READY)) {
                this.translate.get('page-home.title').subscribe((translation:string) => {

                    // Set title of web page in browser
                    this.app.setTitle(translation);
                });
            }
        }));

        this.autorun(() => {
            let path:string = Session.get(Constants.SESSION.PATH);
            if (path) {
                switch (path) {
                    case Constants.PATHS.CREATE_ACCOUNT:
                        console.log("PATH: ", Constants.PATHS.CREATE_ACCOUNT);
                        this.nav.push(CreateAccountPage);
                        break;
                }
            }
            window.history.pushState('', document.title, Meteor.absoluteUrl());
        });
    }
}