import {Component, OnInit, NgZone} from '@angular/core';
import {App, NavController} from 'ionic-angular';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from 'ng2-translate';

import {Constants} from "../../../../../both/Constants";

import template from './developer.html';
import {INavigationMenuPage} from "../../app.component";
import {BillingAccountPage} from "./billing-account/billing-account";
import {ProjectsPage} from "./projects/projects";
@Component({
    selector: 'page-developer',
    template: template
})
export class DeveloperPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public user:Meteor.User;
    public pages:Array<INavigationMenuPage>;

    constructor(public app:App,
                public nav:NavController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => {
            this.user = Meteor.user();
        });

        this.pages = [{
            icon: "apps",
            title: 'page-developer.pages.projects',
            component: ProjectsPage
        }, {
            icon: "briefcase",
            title: 'page-developer.pages.billingAccount',
            component: BillingAccountPage
        }];
    }

    private openPage(page:INavigationMenuPage) {
        if (page.component) {
            this.nav.push(page.component);
        }
    }
}