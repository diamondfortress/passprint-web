import {Component, OnInit, NgZone} from '@angular/core';
import {NavController} from 'ionic-angular';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from 'ng2-translate';

import {Constants} from "../../../../../../both/Constants";

import template from './billing-account.html';
import {IAddressFormComponent} from "../../../components/address-form/address-form";
import {Address} from "../../../../../../both/models/address.model";
@Component({
    selector: 'page-billing-account',
    template: template
})
export class BillingAccountPage extends MeteorComponent implements OnInit, IAddressFormComponent {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public user:Meteor.User;
    public addressFormValid:boolean = false;
    public address:Address;

    constructor(public nav:NavController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => {
            this.user = Meteor.user();
        });
        this.address = new Address();
    }

    onAddressFormValueChanged(event) {
        this.zone.run(() => {
            this.addressFormValid = event.valid;
        });
    }
}