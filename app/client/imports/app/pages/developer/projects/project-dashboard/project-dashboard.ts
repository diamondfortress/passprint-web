import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../../../both/Constants";
import template from "./project-dashboard.html";
import {Project} from "../../../../../../../both/models/project.model";
import {IOAuthClient} from "../../../../../../../both/models/oauth2-client.model";
import {OAuthClientsCollection} from "../../../../../../../both/collections/oauth2.collections";
import * as moment from "moment";
import {IPricing} from "../../../../../../../both/models/pricing.model";
@Component({
    selector: 'page-project-dashboard',
    template: template
})
export class ProjectDashboardPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public placeholderImageUri:string = Constants.ADD_IMAGE_PLACEHOLDER_URI;
    public user:Meteor.User;
    public project:Project;
    public oauthClients:Array<IOAuthClient>;
    public aggregates:any = {};
    public projectCurrentMonthGrants:{total:number, days:Array<any>} = {total:0, days:[]};
    public nowString:string = moment().toISOString();
    public billing:IPricing;

    constructor(public nav:NavController,
                public params:NavParams,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        Session.set(Constants.SESSION.LOADING, true);
        this.project = new Project(this.params.data);
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
        }));

        this.subscribe(Constants.PUBLICATIONS.PROJECT_CREDENTIALS, this.project._id, () => {
            this.autorun(() => this.zone.run(() => {
                this.oauthClients = OAuthClientsCollection.find({
                    projectId: this.project._id
                }).fetch();
                //console.log("oauthClients: ", this.oauthClients);

                if (!this.oauthClients) {
                    Session.set(Constants.SESSION.LOADING, false);
                } else {
                    let projectClientIds:Array<string> = this.oauthClients.map((client:IOAuthClient) => {
                        return client._id;
                    });

                    Meteor.call("projectClientGrantsAggregate", {projectClientIds: projectClientIds}, (error, result) => {
                        if (error) {
                            Session.set(Constants.SESSION.LOADING, false);
                            console.error("Error retrieving program's clients' grants: ", error);
                        } else {
                            //console.log("programClientGrants: ", result);
                            this.aggregates.grants = result;
                            //console.log("Aggregates: ", this.aggregates);
                            this.joinData();
                        }
                    });
                    
                    let now = moment(this.nowString);
                    Meteor.call("getProjectUsersByMonth", {
                        projectClientIds: projectClientIds,
                        year: now.year(),
                        month: now.month()
                    }, (error, result) => {
                        if (error) {
                            Session.set(Constants.SESSION.LOADING, false);
                            console.error("Error retrieving program's monthly users: ", error);
                        } else {
                            //console.log("programClientGrants: ", result);
                            this.aggregates.users = {currentMonth: result[0]};
                            //console.log("Aggregates: ", this.aggregates);
                            this.joinData();
                        }
                    });

                    Meteor.call("getProjectMonthlyBilling", {
                        projectClientIds: projectClientIds,
                        year: now.year(),
                        month: now.month()
                    }, (error, result) => {
                        if (error) {
                            Session.set(Constants.SESSION.LOADING, false);
                            console.error("Error retrieving program's monthly billing: ", error);
                        } else {
                            this.billing = result;
                        }
                    });
                }
            }));
        });
    }

    private joinData():void {
        if (this.oauthClients && this.aggregates) {
            this.oauthClients.forEach((client:IOAuthClient) => {
                let dailyGrants:Array<any> = this.aggregates.grants.clients.daily.filter((grants:any) => {
                    return grants._id.clientId === client._id;
                });
                let clientGrants:any = this.aggregates.grants.clients.totals.find((grants:any) => {
                    return grants._id === client._id;
                });
                client["grants"] = {
                    total:0,
                    daily:[]
                };
                if (dailyGrants && Array.isArray(dailyGrants)) {
                    client["grants"]["daily"] = dailyGrants;
                }
                if (clientGrants) {
                    client["grants"]["total"] = clientGrants.clientGrants;
                }
            });
            //console.log("oauthClientData: ", this.oauthClients);

            var now = moment(this.nowString);
            let projectCurrentMonthGrants = this.aggregates.grants.project.daily.filter((grant:any) => {
                return now.year() === grant._id.year && now.month() === grant._id.month;
            });
            var projectCurrentMonthGrantsTotal:number = 0;
            projectCurrentMonthGrants.forEach((day:any) => {
                projectCurrentMonthGrantsTotal += day.dailyGrants;
            });
            this.projectCurrentMonthGrants = {
                total: projectCurrentMonthGrantsTotal,
                days: projectCurrentMonthGrants.sort(this.sortGrantsByDate.bind(this))
            };
            //console.log("projectCurrentMonthGrants: ", projectCurrentMonthGrants);
        }

        Session.set(Constants.SESSION.LOADING, false);
    }

    private sortGrantsByDate(a, b):number {
        let sortDirection:number = 0;
        let aMoment = moment(a._id.year + "-" + a._id.month + "-" + a._id.day, "YYYY-MM-DD");
        let bMoment = moment(b._id.year + "-" + b._id.month + "-" + b._id.day, "YYYY-MM-DD");
        if (aMoment.diff(bMoment) > 0) {
            sortDirection = 1;
        } else if (aMoment.diff(bMoment) < 0) {
            sortDirection = -1;
        }
        return sortDirection;
    }
}