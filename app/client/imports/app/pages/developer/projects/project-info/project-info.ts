import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, AlertController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../../../both/Constants";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import template from "./project-info.html";
import {FormValidator} from "../../../../utils/FormValidator";
import {Project, IProject} from "../../../../../../../both/models/project.model";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
@Component({
    selector: 'page-project-info',
    template: template
})
export class ProjectInfoPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public user:Meteor.User;
    public project:Project;
    public nameForm:FormGroup;
    public nameFormControl:{
        displayName:AbstractControl
    };
    public urlForm:FormGroup;
    public urlFormControl:{
        homepageUrl:AbstractControl,
        privacyPolicyUrl:AbstractControl,
        tosUrl:AbstractControl
    };
    public imageUri:string;
    public placeholderImageUri:string = Constants.ADD_IMAGE_PLACEHOLDER_URI;
    public tracker:any;
    public initImageUri:boolean = true;


    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService,
                public fb:FormBuilder) {
        super();
    }

    ngOnInit() {
        let project:IProject = this.params.data;
        if (!project._id) {
            project.userId = Meteor.userId();
        }
        this.project = new Project(project);
        this.autorun(() => {
            this.user = Meteor.user();
        });

        this.nameForm = this.fb.group({
            'displayName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])]
        });

        this.nameFormControl = {
            displayName: this.nameForm.controls['displayName']
        };

        this.urlForm = this.fb.group({
            'homepageUrl': [Constants.EMPTY_STRING, Validators.compose([
                FormValidator.validUrl
            ])],
            'privacyPolicyUrl': [Constants.EMPTY_STRING, Validators.compose([
                FormValidator.validUrl
            ])],
            'tosUrl': [Constants.EMPTY_STRING, Validators.compose([
                FormValidator.validUrl
            ])]
        });

        this.urlFormControl = {
            homepageUrl: this.urlForm.controls['homepageUrl'],
            privacyPolicyUrl: this.urlForm.controls['privacyPolicyUrl'],
            tosUrl: this.urlForm.controls['tosUrl']
        };

        this.tracker = Tracker.autorun(() => this.zone.run(() => {
            if (this.initImageUri) {
                this.initImageUri = false;
                Session.set(Constants.SESSION.imageUri, this.project.logo);
            }
            this.imageUri = Session.get(Constants.SESSION.imageUri);
        }));
    }
    
    private save():void {
        var self = this;
        if (self.nameForm.valid && self.urlForm.valid) {
            self.project.logo = Session.get(Constants.SESSION.imageUri);
            Meteor.call("saveProjectInfo", self.project, (error, result) => {
                if (error) {
                    let alert = self.alertCtrl.create({
                        title: self.translate.instant("page-project-info.errors.projectInfoError"),
                        subTitle: self.translate.instant("page-project-info.errors.saveProjectInfo"),
                        message: error.reason || error.message || error,
                        buttons: [{
                            text: self.translate.instant("general.ok")
                        }]
                    });
                    alert.present();
                } else {
                    console.log("saveProjectInfo() result: ", result);
                    new ToastMessenger().toast({
                        type: "success",
                        message: self.translate.instant("page-project-info.toasts.savedProjectInfo")
                    });
                    self.nav.pop();
                }
            });
        }
    }

    ionViewWillLeave() {
        // stop tracker so item does not continue to update
        this.tracker.stop();
    }
}