import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../../../both/Constants";
import template from "./project-menu.html";
import {Project, IProject} from "../../../../../../../both/models/project.model";
import {INavigationMenuPage} from "../../../../app.component";
import {ProjectInfoPage} from "../project-info/project-info";
import {ProjectsCollection} from "../../../../../../../both/collections/projects.collection";
import {ProjectCredentialsPage} from "../project-credentials/project-credentials";
import {ProjectDashboardPage} from "../project-dashboard/project-dashboard";
@Component({
    selector: 'page-project-menu',
    template: template
})
export class ProjectMenuPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public placeholderImageUri:string = Constants.ADD_IMAGE_PLACEHOLDER_URI;
    public user:Meteor.User;
    public project:Project;
    public pages:Array<INavigationMenuPage>;

    constructor(public nav:NavController,
                public params:NavParams,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.project = new Project(this.params.data);
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.subscribe(Constants.PUBLICATIONS.MY_PROJECTS, () => {
                    this.autorun(() => this.zone.run(() => {
                        if (this.user) {
                            let project:IProject = ProjectsCollection.findOne({
                                _id: this.project._id,
                                userId: this.user._id,
                            });
                            this.project = new Project(project);
                        }
                    }));
                });
            }
        });
        this.pages = [{
            icon: "information-circle",
            title: this.translate.instant("page-project-info.title"),
            component: ProjectInfoPage
        }, {
            icon: "fa-tachometer",
            isIconFA: true,
            title: this.translate.instant("general.dashboard"),
            component: ProjectDashboardPage
        }, {
            icon: "fa-id-card-o",
            isIconFA: true,
            title: this.translate.instant("page-project-menu.credentials"),
            component: ProjectCredentialsPage
        }];
    }

    private openPage(page:INavigationMenuPage):void {
        if (page.component) {
            this.nav.push(page.component, this.project);
        }
    }
}