import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, AlertController, ModalController, Modal} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {Constants} from "../../../../../../../../both/Constants";
import * as moment from "moment";
import template from "./edit-oauth-client.html";
import {OAuthClient, IOAuthClient} from "../../../../../../../../both/models/oauth2-client.model";
import {ToastMessenger} from "../../../../../utils/ToastMessenger";
import {AuthorizedOriginModalPage} from "./authorized-origin-modal/authorized-origin-modal";
import {AuthorizedRedirectModalPage} from "./authorized-redirect-modal/authorized-redirect-modal";
@Component({
    selector: 'page-edit-oauth-client',
    template: template
})
export class EditOAuthClientPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public user:Meteor.User;
    public date = moment().toISOString();
    public oauthClient:OAuthClient;
    public nameForm:FormGroup;
    public nameFormControl:{
        clientName:AbstractControl
    };

    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService, 
                public fb:FormBuilder) {
        super();
    }

    ngOnInit() {
        let oauthClient:IOAuthClient = this.params.data;
        this.oauthClient = new OAuthClient(oauthClient);
        this.autorun(() => {
            this.user = Meteor.user();
        });

        this.nameForm = this.fb.group({
            'clientName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])]
        });

        this.nameFormControl = {
            clientName: this.nameForm.controls['clientName']
        };
    }

    private promptRefreshSecretKey():void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-edit-oauth-client.alerts.refreshSecret.title"),
            message: self.translate.instant("page-edit-oauth-client.alerts.refreshSecret.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.refresh"),
                handler: () => {
                    self.refreshClientSecret();
                }
            }]
        });
        alert.present();
    }
    
    private editAuthorizedOrigin(origin:string, index?:number):void {
        var self = this;
        let modal:Modal = self.modalCtrl.create(AuthorizedOriginModalPage, {origin: origin});
        modal.onDidDismiss((url:string) => {
            if (url) {
                if (origin && index >= 0) {
                    self.oauthClient.origins[index] = url;
                } else {
                    self.oauthClient.origins.push(url);
                }
            }
        });
        modal.present();
    }
    
    private removeAuthorizedOrigin(index:number):void {
        this.oauthClient.origins.splice(index, 1);
    }
    
    private editAuthorizedRedirectUri(redirectUri:string, index?:number):void {
        var self = this;
        let modal:Modal = self.modalCtrl.create(AuthorizedRedirectModalPage, {redirectUri: redirectUri});
        modal.onDidDismiss((url:string) => {
            if (url) {
                if (redirectUri && index >= 0) {
                    self.oauthClient.redirectUri[index] = url;
                } else {
                    self.oauthClient.redirectUri.push(url);
                }
            }
        });
        modal.present();
    }

    private removeAuthorizedRedirectUri(index:number):void {
        this.oauthClient.redirectUri.splice(index, 1);
    }

    private refreshClientSecret():void {
        var self = this;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("refreshOAuthClientSecret", {clientId: self.oauthClient._id}, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("refreshOAuthClientSecret() error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-edit-oauth-client.errors.oauthClient"),
                    message: error.reason || error.message || error,
                    buttons: [{text: self.translate.instant("general.ok")}]
                });
                alert.present();
            } else {
                console.log("refreshOAuthClientSecret() result: ", result);
                let secret:string = result.secret;
                this.zone.run(() => {
                    this.oauthClient.clientSecret = secret;
                });
            }
        });
    }
    
    private save():void {
        var self = this;
        if (self.nameForm.valid) {
            Session.set(Constants.SESSION.LOADING, true);
            Meteor.call("saveOAuthClient", self.oauthClient, (error, result) => {
                Session.set(Constants.SESSION.LOADING, false);
                if (error) {
                    console.error("refreshOAuthClientSecret() error: ", error);
                    let alert = self.alertCtrl.create({
                        title: self.translate.instant("page-edit-oauth-client.errors.oauthClient"),
                        message: error.reason || error.message || error,
                        buttons: [{text: self.translate.instant("general.ok")}]
                    });
                    alert.present();
                } else {
                    new ToastMessenger().toast({
                        type: "success",
                        message: self.translate.instant("page-edit-oauth-client.toasts.savedOAuthClient")
                    });
                    self.nav.pop();
                }
            });
        }
    }
}