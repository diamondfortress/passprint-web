import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, ViewController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../../../../../both/Constants";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import template from "./authorized-redirect-modal.html";
import {FormValidator} from "../../../../../../utils/FormValidator";
@Component({
    selector: 'page-authorized-origin-modal',
    template: template
})
export class AuthorizedRedirectModalPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public user:Meteor.User;
    public url:string;
    public formGroup:FormGroup;
    public formControl:{
        url:AbstractControl
    };

    constructor(public nav:NavController,
                public params:NavParams,
                public viewCtrl:ViewController,
                public zone:NgZone,
                public translate:TranslateService,
                public fb:FormBuilder) {
        super();
    }

    ngOnInit() {
        this.url = this.params.data.redirectUri || Constants.EMPTY_STRING;
        this.autorun(() => {
            this.user = Meteor.user();
        });

        this.formGroup = this.fb.group({
            'url': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.validUrl
            ])]
        });

        this.formControl = {
            url: this.formGroup.controls['url']
        };
    }

    private dismiss():void {
        this.viewCtrl.dismiss();
    }

    private save():void {
        if (this.formGroup.valid) {
            this.viewCtrl.dismiss(this.url);
        }
    }
}