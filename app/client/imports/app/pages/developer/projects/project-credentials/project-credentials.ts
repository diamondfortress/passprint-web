import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, AlertController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../../../both/Constants";
import template from "./project-credentials.html";
import {IProject, Project} from "../../../../../../../both/models/project.model";
import {EditOAuthClientPage} from "./edit-oauth-client/edit-oauth-client";
import {OAuthClientsCollection} from "../../../../../../../both/collections/oauth2.collections";
import {IOAuthClient, OAuthClient} from "../../../../../../../both/models/oauth2-client.model";
@Component({
    selector: 'page-project-credentials',
    template: template
})
export class ProjectCredentialsPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public user:Meteor.User;
    public project:Project;
    public oauthClients:Array<IOAuthClient>;

    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        let project:IProject = this.params.data;
        this.project = new Project(project);
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.subscribe(Constants.PUBLICATIONS.PROJECT_CREDENTIALS, this.project._id, () => {
                    this.autorun(() => this.zone.run(() => {
                        if (this.user) {
                            this.oauthClients = OAuthClientsCollection.find({
                                userId: this.user._id,
                                projectId: this.project._id
                            }).fetch();
                            console.log("oauthClients: ", this.oauthClients);
                        }
                    }));
                });
            }
        });
    }
    
    private editCredentials(client:IOAuthClient):void {
        if (!client) {
            client = {
                userId: this.user._id,
                projectId: this.project._id
            };
        }
        let oauthClient:OAuthClient = new OAuthClient(client);
        this.nav.push(EditOAuthClientPage, oauthClient)
    }

    private confirmDeleteClient(client:IOAuthClient):void {
        var self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-project-credentials.alerts.confirmDeleteClient.title"),
            subTitle: client.clientName,
            message: self.translate.instant("page-project-credentials.alerts.confirmDeleteClient.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.delete"),
                handler: () => {
                    self.deleteOAuthClient(client);
                }
            }]
        });
        alert.present();
    }

    private deleteOAuthClient(client:IOAuthClient):void {
        var self = this;
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call("deleteOAuthClient", {clientId: client._id}, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("deleteOAuthClient() error: ", error);
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-edit-oauth-client.errors.oauthClient"),
                    message: error.reason || error.message || error,
                    buttons: [{text: self.translate.instant("general.ok")}]
                });
                alert.present();
            } else {
                console.log("deleteOAuthClient() result: ", result);
            }
        });
    }
}