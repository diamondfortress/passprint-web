import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, ModalController} from "ionic-angular";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "ng2-translate";
import {Constants} from "../../../../../../both/Constants";
import template from "./projects.html";
import {ProjectInfoPage} from "./project-info/project-info";
import {ProjectsCollection} from "../../../../../../both/collections/projects.collection";
import {IProject} from "../../../../../../both/models/project.model";
import {ProjectMenuPage} from "./project-menu/project-menu";
@Component({
    selector: 'page-projects',
    template: template
})
export class ProjectsPage extends MeteorComponent implements OnInit {
    public navbarPassPrintLogoUri:string = Constants.NAVBAR_PASSPRINT_LOGO_URI;
    public placeholderImageUri:string = Constants.ADD_IMAGE_PLACEHOLDER_URI;
    public user:Meteor.User;
    public projects:Array<IProject>;

    constructor(public nav:NavController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.subscribe(Constants.PUBLICATIONS.MY_PROJECTS, () => {
                    this.autorun(() => this.zone.run(() => {
                        if (this.user) {
                            this.projects = ProjectsCollection.find({userId: this.user._id}).fetch();
                        }
                    }));
                });
            }
        });
    }

    private createProject():void {
        this.nav.push(ProjectInfoPage);
    }
    
    private openProjectMenu(project:IProject):void {
        this.nav.push(ProjectMenuPage, project);
    }
}