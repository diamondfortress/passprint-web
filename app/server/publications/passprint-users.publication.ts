import {Constants} from "../../both/Constants";
declare var Roles;
Meteor.publish(Constants.PUBLICATIONS.PASSPRINT_USERS, function (authReqId:string) {
    if (Roles.userIsInRole(this.userId, [Constants.ROLES.ADMIN], Constants.PASSPRINT_ROLES_GROUP)) {
        let query:any = {};
        let querySafeGroupName:string = (Constants.PASSPRINT_ROLES_GROUP).replace(/\./g, "_");
        let querySelector:string = "roles." + querySafeGroupName;
        query[querySelector] = {$in: [Constants.ROLES.USER]};
        return Meteor.users.find(query, {fields: {emails:1, profile:1, roles:1}});
    }
});