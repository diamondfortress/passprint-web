import {Constants} from "../../both/Constants";
import {ProjectsCollection} from "../../both/collections/projects.collection";
Meteor.publish(Constants.PUBLICATIONS.MY_PROJECTS, function (authReqId:string) {
    return ProjectsCollection.find({userId: this.userId});
});