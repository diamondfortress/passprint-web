import {Constants} from "../../both/Constants";
import {OAuthClientsCollection} from "../../both/collections/oauth2.collections";
import {IOAuthClient} from "../../both/models/oauth2-client.model";
import {AuthenticationRequestsCollection} from "../../both/collections/authentication-requests.collection";
Meteor.publish(Constants.PUBLICATIONS.PROJECT_AUTHENTICATIONS, function (projectId:string) {

    let projectClients:Array<IOAuthClient> = OAuthClientsCollection.find({
        userId: this.userId,
        projectId: projectId
    }, {
        fields: {
            _id: 1
        }
    }).fetch();
    if (projectClients) {
        let programClientIds:Array<string> = projectClients.map((client:IOAuthClient) => {
            return client._id;
        });
        return AuthenticationRequestsCollection.find({
            clientId: {
                $in: programClientIds
            },
            isVerified: true
        }, {
            fields: {
                loginToken: 0
            }
        });
    }
});