import {Constants} from "../../both/Constants";
import {OAuthClientsCollection, OAuth2GrantsCollection} from "../../both/collections/oauth2.collections";
import {IOAuthClient} from "../../both/models/oauth2-client.model";
Meteor.publish(Constants.PUBLICATIONS.PROJECT_CLIENT_GRANTS, function (projectId:string) {
    let projectClients:Array<IOAuthClient> = OAuthClientsCollection.find({
        userId: this.userId,
        projectId: projectId
    }, {
        fields: {
            _id: 1
        }
    }).fetch();
    if (projectClients) {
        let programClientIds:Array<string> = projectClients.map((client:IOAuthClient) => {
            return client._id;
        });
        return OAuth2GrantsCollection.find({
            clientId: {
                $in: programClientIds
            }
        });
    }
});