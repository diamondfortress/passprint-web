import {Constants} from "../../both/Constants";
import {OAuthClientsCollection} from "../../both/collections/oauth2.collections";
declare var Roles;
Meteor.publish(Constants.PUBLICATIONS.RELYING_PARTIES, function (authReqId:string) {
    if (Roles.userIsInRole(this.userId, [Constants.ROLES.ADMIN], Constants.PASSPRINT_ROLES_GROUP)) {
        return OAuthClientsCollection.find({
            clientId: {
                $nin: [Meteor.settings.private["passprintClientId"]]
            }
        });
    }
});