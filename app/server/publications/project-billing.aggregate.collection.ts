import {Constants} from "../../both/Constants";
import {IOAuthClient} from "../../both/models/oauth2-client.model";
import {OAuthClientsCollection} from "../../both/collections/oauth2.collections";

// Meteor.publish(Constants.PUBLICATIONS.PROJECT_BILLING_AGGREGATE, function (projectId:string) {
//     let projectClients:Array<IOAuthClient> = OAuthClientsCollection.find({
//         projectId: projectId
//     }, {
//         fields: {
//             _id: 1
//         }
//     }).fetch();
//     if (projectClients) {
//         let programClientIds:Array<string> = projectClients.map((client:IOAuthClient) => {
//             return client._id;
//         });
//         var pipeline:any = [{
//             $match: {
//                 clientId: {
//                     $in: programClientIds
//                 }
//             }
//         }, {
//             $group: {
//                 _id: {
//                     clientId: "$clientId",
//                     year: {"$year": "$created"},
//                     month: {"$month": "$created"},
//                     day: {"$dayOfMonth": "$created"}
//                 },
//                 count: {
//                     $sum: 1
//                 }
//             }
//         }];
//     }
// });