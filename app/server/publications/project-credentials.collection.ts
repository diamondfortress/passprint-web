import {Constants} from "../../both/Constants";
import {OAuthClientsCollection} from "../../both/collections/oauth2.collections";
Meteor.publish(Constants.PUBLICATIONS.PROJECT_CREDENTIALS, function (projectId:string) {
    return OAuthClientsCollection.find({userId: this.userId, projectId: projectId});
});