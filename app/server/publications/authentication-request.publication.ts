import {AuthenticationRequestsCollection} from "../../both/collections/authentication-requests.collection";
import {Constants} from "../../both/Constants";
Meteor.publish(Constants.PUBLICATIONS.AUTHENTICATION_REQUESTS, function (authReqId:string) {
    return AuthenticationRequestsCollection.find({_id: authReqId});
});