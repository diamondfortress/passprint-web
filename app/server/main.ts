import { Main } from "./imports/server-main/main";
import './publications/authentication-request.publication.ts'

const mainInstance = new Main();
mainInstance.start();
