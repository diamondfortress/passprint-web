import {OauthServiceConfig} from "../../lib/oauth-service-config";
import {MeteorMethods} from "../methods/methods";
import {IOAuthClient, OAuthClient} from "../../../both/models/oauth2-client.model";
import {Constants} from "../../../both/Constants";
import {OAuthClientsCollection, OAuth2GrantsCollection} from "../../../both/collections/oauth2.collections";
import {IProject, Project} from "../../../both/models/project.model";
import {ProjectsCollection} from "../../../both/collections/projects.collection";
import * as moment from "moment";
import {OAuth2Grant, IOAuth2Grant} from "../../../both/models/oauth2-grant.model";
import {IPricing} from "../../../both/models/pricing.model";
import {PricingCollection} from "../../../both/collections/pricing.collection";
declare var process;
declare var Roles;

export class Main {
    start():void {
        this.printSettings();
        this.initData();

        var oauthProviderConfig = new OauthServiceConfig();
        oauthProviderConfig.initOauthServices();
        
        var meteorMethods = new MeteorMethods();
        meteorMethods.init();
    }

    private initData():void {
        let passprintClientId:string = Meteor.settings.private["passprintClientId"];
        let passprintProject:IProject = ProjectsCollection.findOne({
            userId: passprintClientId
        });
        let passprintClient:IOAuthClient = OAuthClientsCollection.findOne({
            clientId: passprintClientId
        });
        if (!passprintProject) {
            var project:Project = new Project({
                userId: passprintClientId,
                displayName: Meteor.settings.public["appName"]
            });
            delete project._id;
            console.log("Create PassPrint Project");
            ProjectsCollection.insert(project, (error, projectId) => {
                if (error) {
                    console.error("Error inserting project: ", error);
                } else {
                    console.log("Successfully created PassPrint Project");
                    var client:IOAuthClient = {
                        userId: passprintClientId,
                        projectId: projectId,
                        clientId: passprintClientId,
                        redirectUri: ["https://local-www.passprint.me/_oauth/passprint"],
                        origins: ["https://local-www.passprint.me"],
                        clientSecret: "use-your-fingerprint-as-your-password",
                        clientName: Meteor.settings.public["appName"]
                    };
                    let oauthClient:OAuthClient = new OAuthClient(client);
                    delete oauthClient._id;
                    console.log("Create PassPrint Client: ", oauthClient);
                    OAuthClientsCollection.insert(oauthClient, (error, clientId) => {
                        if (error) {
                            console.error("Error inserting client: ", error);
                        } else {
                            console.log("Successfully create PassPrint Client");
                        }
                    });
                }
            });
        }

        // let admin:Meteor.User = Accounts.findUserByEmail("mwheatley@diamondfortress.com");
        // Roles.addUsersToRoles(admin._id, [Constants.ROLES.ADMIN], Constants.PASSPRINT_ROLES_GROUP);

        // if (OAuth2GrantsCollection.find().count() < 2500) {
        //     let date = moment("2016-05-28", "YYYY-MM-DD");
        //     let endDate = moment("2017-09-14", "YYYY-MM-DD");
        //     console.log("startDate: ", date.toISOString());
        //     let oauthGrant:OAuth2Grant = new OAuth2Grant({
        //         userId: new Mongo.ObjectID().valueOf().toString(),
        //         clientId: "YcFYJMtqnPzWhRaY2",
        //         created: new Date(date.toISOString())
        //     });
        //     delete oauthGrant._id;
        //     while (date <= endDate) {
        //         let randomNum1 = (Math.floor(Math.random() * 9) + 1  );
        //         let randomNum2 = (Math.floor(Math.random() * 9) + 1  );
        //         for (let i = 0; i < randomNum2; i++) {
        //             date.add(randomNum2, "hours");
        //             console.log("date: ", date.toISOString());
        //             oauthGrant.created = new Date(date.toISOString());
        //             OAuth2GrantsCollection.insert(oauthGrant);
        //         }
        //
        //         date.add(randomNum1, "days");
        //     }
        // }

        let baseRate:number = 0.25;
        let increment:number = 1000;
        let discount:number = 0.01;
        let basementRate:number = 0.10;
        let pricing:IPricing = {
            increment: increment,
            levels: []
        };
        let currentRate:number = baseRate;
        let users:number = 0;
        for (currentRate; currentRate >= basementRate; currentRate = parseFloat((currentRate - discount).toFixed(2))) {
            pricing.levels.push({
                users: users,
                rate: currentRate
            });
            users += increment;
        }
        console.log("PassPrint Pricing: ", pricing);
        let passprintPricing = PricingCollection.findOne();
        let pricingId = Constants.EMPTY_STRING;
        if (passprintPricing) {
            pricingId = passprintPricing._id;
        }
        PricingCollection.upsert({_id: pricingId}, pricing);
    }

    private printSettings():void {
        console.log("process.env.ROOT_URL: " + process.env.ROOT_URL);
        console.log("process.env.MOBILE_DDP_URL: " + process.env.MOBILE_DDP_URL);
        console.log("process.env.MOBILE_ROOT_URL: " + process.env.MOBILE_ROOT_URL);
        console.log("process.env.MONGO_URL: ", + process.env.MONGO_URL);
        console.log("process.env.METEOR_SETTINGS: " + process.env.METEOR_SETTINGS);
        if (!process.env.METEOR_SETTINGS) {
            console.log("No METEOR_SETTINGS found.  Please restart the app with the METEOR_SETTINGS environment variable set.")
        }
    }
}
