import {Constants} from "../../../both/Constants";
import {AuthenticationRequestsCollection} from "../../../both/collections/authentication-requests.collection";
import {AuthenticationRequest} from "../../../both/models/authentication-request.model";
import {OAuthClientsCollection, OAuth2GrantsCollection} from "../../../both/collections/oauth2.collections";
import {IProject} from "../../../both/models/project.model";
import {ProjectsCollection} from "../../../both/collections/projects.collection";
import {IOAuthClient} from "../../../both/models/oauth2-client.model";
import {IPricing, IPriceTier} from "../../../both/models/pricing.model";
import {PricingCollection} from "../../../both/collections/pricing.collection";
var Future = Npm.require('fibers/future');

declare var console;
declare var Accounts;
declare var Roles;

export class MeteorMethods {

    public init():void {
        Meteor.methods({
            'sample': () => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                if (user) {
                    var future = new Future();
                    try {
                        return future.wait();
                    } catch (error) {
                        throw error;
                    }
                }
            },
            'getAccounts': (accounts:Array<string>) => {
                return Meteor.users.find({_id: {$in: accounts}}, {fields: {emails: 1, profile: 1}}).fetch();
            },
            'getAccountForLogin': (data:{email:string}) => {
                var user:Meteor.User = this.checkAccountRegistration(data.email);
                return Meteor.users.findOne({
                    "emails.address": data.email
                }, {
                    fields: {
                        emails: 1,
                        profile: 1
                    }
                });
            },
            'loginWithPassPrint': (data:{
                email:string
            }) => {
                var self = this;
                var user:Meteor.User = Accounts.findUserByEmail(data.email);
                if (!user) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Email not found.");
                } else {
                    if (!user["fingerprintIds"] || user["fingerprintIds"].length === 0) {
                        throw new Meteor.Error(Constants.METEOR_ERRORS.REGISTRATION_INCOMPLETE,
                            "Account registration incomplete.")
                    }

                    var client:any = OAuthClientsCollection.findOne({
                        clientId: Meteor.settings.private["passprintClientId"]
                    }, {
                        fields: {
                            _id: 1
                        }
                    });

                    if (!client) {
                        throw new Meteor.Error(Constants.METEOR_ERRORS.OAUTH2_CLIENT_NOT_FOUND,
                            "PassPrint Client not found.")
                    }

                    var authReq:AuthenticationRequest = {
                        userId: user._id,
                        clientId: client._id
                    };
                    var authReqId:string = AuthenticationRequestsCollection.insert(authReq);

                    Meteor.users.update({_id: user._id}, {$set: {authenticationRequest: authReqId}});

                    var passprintPushServiceEndpoint:string = Meteor.settings.private["push"]["basePath"] +
                        Constants.PASSPRINT_PUSH_ENDPOINTS.AUTHENTICATE;

                    var postData:any = {
                        api_key: Meteor.settings.private["push"]["apiKey"],
                        authReqId: authReqId
                    };
                    var future = new Future();
                    HTTP.call("POST", passprintPushServiceEndpoint, {
                        data: postData,
                        timeout: 20 * 1000
                    }, function (error, result) {
                        if (error) {
                            future.throw(error)
                        } else {
                            future.return({authReqId: authReqId});
                        }
                    });

                    try {
                        return future.wait();
                    } catch (error) {
                        console.log("PassPrint Push Error: ", error);
                        if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                            error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                        }
                        throw error;
                    }
                }
            },
            'checkAccountRegistration': (data:{
                email:string
            }) => {
                this.checkAccountRegistration(data.email);
                return true;
            },
            'createAccount': (user:any) => {
                var existingUser:Meteor.User = Accounts.findUserByEmail(user.email);
                if (existingUser) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.EMAIL_EXISTS, "Email already exists.");
                }

                user.profile.name.display = user.profile.name.given + " " + user.profile.name.family;
                var userId:string = Accounts.createUser(user);
                console.log("new user ID: ", userId);

                if (userId) {
                    Roles.addUsersToRoles(userId, [Constants.ROLES.USER], Constants.PASSPRINT_ROLES_GROUP);
                    Accounts.sendResetPasswordEmail(userId);
                }
                return {userId: userId};
            },
            'sendVerificationEmail': (data:{email:string}) => {
                var existingUser:Meteor.User = Accounts.findUserByEmail(data.email);
                if (!existingUser) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Email not registered");
                } else {
                    var isEnrolled:boolean = (existingUser["fingerprintIds"] &&
                    existingUser["fingerprintIds"].length > 0);
                    if (isEnrolled) {
                        throw new Meteor.Error(Constants.METEOR_ERRORS.REGISTRATION_COMPLETE,
                            "Account registration complete.");
                    } else {
                        Accounts.sendResetPasswordEmail(existingUser._id);
                    }
                }
            },
            'toggleAdminRole': (data:{userId:string}) => {
                var currentUser:Meteor.User = this.checkForUser();
                let currentUserIsAdmin:boolean = Roles.userIsInRole(
                    currentUser._id,
                    [Constants.ROLES.ADMIN],
                    Constants.PASSPRINT_ROLES_GROUP
                );
                if (!currentUserIsAdmin) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCESS_DENIED, "Admins only");
                }
                var selectedUser:Meteor.User = Meteor.users.findOne({_id: data.userId});
                if (!selectedUser) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid user ID");
                }
                let selectedUserIsAdmin:boolean = Roles.userIsInRole(
                    selectedUser._id,
                    [Constants.ROLES.ADMIN],
                    Constants.PASSPRINT_ROLES_GROUP
                );
                if (!selectedUserIsAdmin) {
                    Roles.addUsersToRoles(selectedUser._id, [Constants.ROLES.ADMIN], Constants.PASSPRINT_ROLES_GROUP);
                } else {
                    let querySafeGroupName:string = (Constants.PASSPRINT_ROLES_GROUP).replace(/\./g, "_");
                    let selectedUserRoles = selectedUser["roles"][querySafeGroupName];
                    var filteredRoles = selectedUserRoles.filter(role => role !== Constants.ROLES.ADMIN);
                    Roles.setUserRoles(selectedUser._id, filteredRoles, Constants.PASSPRINT_ROLES_GROUP);
                }
                return {success: true, message: "Successfully toggled admin status."};
            },
            'removeUser': (data:{userId:string}) => {
                var currentUser:Meteor.User = this.checkForUser();
                let currentUserIsAdmin:boolean = Roles.userIsInRole(
                    currentUser._id,
                    [Constants.ROLES.ADMIN],
                    Constants.PASSPRINT_ROLES_GROUP
                );
                if (!currentUserIsAdmin) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCESS_DENIED, "Admins only");
                }
                var selectedUser:Meteor.User = Meteor.users.findOne({_id: data.userId});
                if (!selectedUser) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid user ID");
                }
                Meteor.users.remove({_id: data.userId});
                return {success: true, message: "Successfully removed user."};
            },
            'saveProjectInfo': (data:IProject) => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                if (user) {
                    var future = new Future();

                    ProjectsCollection.upsert({_id: data._id}, data, (error, result) => {
                        if (error) {
                            console.log("Error saving project info: ", error);
                            future.throw(error);
                        } else {
                            console.log("Successfully saved project info: ", result);
                            future.return(result);
                        }
                    });

                    try {
                        return future.wait();
                    } catch (error) {
                        throw error;
                    }
                }
            },
            'refreshOAuthClientSecret': (data:{clientId:string}) => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                if (user) {
                    var future = new Future();
                    var oauthClient:IOAuthClient = OAuthClientsCollection.findOne({_id: data.clientId});
                    if (!oauthClient) {
                        throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid clientId");
                    }
                    var secret:string = Random.secret();
                    oauthClient.clientSecret = secret;
                    OAuthClientsCollection.upsert({_id: data.clientId}, oauthClient, (error, result) => {
                        if (error) {
                            console.log("Error refreshing OAuth Client secret: ", error);
                            future.throw(error);
                        } else {
                            console.log("Successfully refreshed OAuth Client secret: ", result);
                            future.return({secret: oauthClient.clientSecret});
                        }
                    });

                    try {
                        return future.wait();
                    } catch (error) {
                        throw error;
                    }
                }
            },
            'saveOAuthClient': (oauthClient:IOAuthClient) => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                if (user) {
                    var future = new Future();
                    OAuthClientsCollection.upsert({_id: oauthClient._id}, oauthClient, (error, result) => {
                        if (error) {
                            console.log("Error saving OAuth Client: ", error);
                            future.throw(error);
                        } else {
                            console.log("Successfully saved OAuth Client: ", result);
                            future.return({secret: oauthClient.clientSecret});
                        }
                    });

                    try {
                        return future.wait();
                    } catch (error) {
                        throw error;
                    }
                }
            },
            'deleteOAuthClient': (data:{clientId}) => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                if (user) {
                    var future = new Future();
                    OAuthClientsCollection.remove({_id: data.clientId}, (error, result) => {
                        if (error) {
                            console.log("Error removing OAuth Client: ", error);
                            future.throw(error);
                        } else {
                            console.log("Successfully removed OAuth Client: ", result);
                            future.return(result);
                        }
                    });

                    try {
                        return future.wait();
                    } catch (error) {
                        throw error;
                    }
                }
            },
            "projectClientGrantsAggregate": (data:{
                projectClientIds:Array<string>
            }) => {
                var clientPipeline:any = [{
                    $match: {
                        clientId: {
                            $in: data.projectClientIds
                        }
                    }
                }, {
                    $sort: {
                        created: -1
                    }
                }, {
                    $group: {
                        _id: {
                            clientId: "$clientId",
                            year: {"$year": "$created"},
                            month: {"$month": "$created"},
                            day: {"$dayOfMonth": "$created"}
                        },
                        dailyGrants: {
                            $sum: 1
                        }
                    }
                }];
                var projectPipeline:any = [{
                    $match: {
                        clientId: {
                            $in: data.projectClientIds
                        }
                    }
                }, {
                    $sort: {
                        created: -1
                    }
                }, {
                    $group: {
                        _id: {
                            year: {"$year": "$created"},
                            month: {"$month": "$created"},
                            day: {"$dayOfMonth": "$created"}
                        },
                        dailyGrants: {
                            $sum: 1
                        }
                    }
                }];
                var clientDailyGrants = (OAuth2GrantsCollection as any).aggregate(clientPipeline);
                var projectDailyGrants = (OAuth2GrantsCollection as any).aggregate(projectPipeline);

                clientPipeline.push({
                    $group: {
                        _id: "$_id.clientId",
                        clientGrants: {
                            $sum: "$dailyGrants"
                        }
                    }
                });

                var clientsGrants = (OAuth2GrantsCollection as any).aggregate(clientPipeline);

                clientPipeline.push({
                    $group: {
                        _id: null,
                        totalGrants: {
                            $sum: "$clientGrants"
                        }
                    }
                });

                var projectGrants = (OAuth2GrantsCollection as any).aggregate(clientPipeline);

                var aggregates:any = {
                    clients: {
                        daily: clientDailyGrants,
                        totals: clientsGrants
                    },
                    project: {
                        daily: projectDailyGrants,
                        totals: projectGrants[0]
                    }
                };
                return aggregates;
            },
            "getProjectUsersByMonth": (data:{
                projectClientIds:Array<string>,
                year:number,
                month:number
            }) => {
                // var allUsersPipeline:Array<any> = [{
                //     $match: {
                //         clientId: {
                //             $in: data.projectClientIds
                //         }
                //     }
                // }, {
                //     $group: {
                //         _id: {
                //             userId: "$userId",
                //             year: {"$year": "$created"},
                //             month: {"$month": "$created"}
                //         }, count: {$sum: 1}
                //     }
                // }, {
                //     $group: {
                //         _id: {
                //             year: "$_id.year",
                //             month: "$_id.month"
                //         }, count: {$sum: 1}
                //     }
                // }, {
                //     $sort: {
                //         "_id.year": 1,
                //         "_id.month": 1
                //     }
                // }];

                var selectedMonthUsersPipeline:Array<any> = [{
                    $project: {
                        userId: 1,
                        clientId: 1,
                        created: 1,
                        year: {$year: "$created"},
                        month: {$month: "$created"}
                    }
                }, {
                    $match: {
                        year: data.year,
                        month: data.month,
                        clientId: {$in: data.projectClientIds}
                    }
                }, {
                    $group: {
                        _id: {
                            userId: "$userId",
                            year: {$year: "$created"},
                            month: {$month: "$created"}
                        },
                        count: {$sum: 1}
                    }
                }, {
                    $group: {
                        _id: {
                            year: "$_id.year",
                            month: "$_id.month"
                        },
                        count: {$sum: 1}
                    }
                }, {
                    $sort: {
                        "_id.year": 1,
                        "_id.month": 1
                    }
                }];
                return (OAuth2GrantsCollection as any).aggregate(selectedMonthUsersPipeline);
            },
            "getProjectMonthlyBilling": (data:{
                projectClientIds:Array<string>,
                year:number,
                month:number
            }) => {
                let result = Meteor.call("getProjectUsersByMonth", {
                    projectClientIds: data.projectClientIds,
                    year: data.year,
                    month: data.month
                });
                console.log("getProjectMonthlyBilling() result: ", result);
                let userCount:number = result[0].count;
                let pricing:IPricing = PricingCollection.findOne();
                let billing:IPricing = {
                    levels: [],
                    total: 0
                };

                for (let index = 0; index < pricing.levels.length; index++) {
                    let priceTier:IPriceTier = pricing.levels[index];
                    console.log("priceTier: ", priceTier);
                    let users:number = 0;
                    if (pricing.levels[index + 1]) {
                        let priceTierUsers:number = pricing.levels[index + 1].users - priceTier.users;
                        console.log("priceTierUsers: ", priceTierUsers);
                        console.log("userCount: ", userCount);
                        let diff = userCount - priceTierUsers;
                        console.log("difference: ", diff);
                        if (diff > 0) {
                            users = priceTierUsers;
                        } else {
                            users = userCount;
                        }
                    } else {
                        users = userCount;
                    }

                    console.log("users at this price tier: ", users);
                    let total = parseFloat((users * priceTier.rate).toFixed(2));
                    billing.levels.push({
                        users: users,
                        rate: pricing.levels[index].rate,
                        total: total
                    });
                    billing.total = parseFloat((billing.total + total).toFixed(2));
                    userCount -= users;
                    console.log("billing: ", billing);
                    if (userCount < 1) {
                        break;
                    }
                }

                return billing;
            }
        });
    }

    private checkForUser():Meteor.User {
        var currentUserId = Meteor.userId();
        var user:Meteor.User;
        if (!currentUserId) {
            throw new Meteor.Error(Constants.METEOR_ERRORS.SIGN_IN, "Please sign in.");
        } else {
            user = Meteor.users.findOne(currentUserId);
            if (!user) {
                throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid User ID");
            }
        }
        return user;
    }

    private checkAccountRegistration(email:string):Meteor.User {
        var user:Meteor.User = Accounts.findUserByEmail(email);
        if (!user) {
            throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Email not found.");
        }
        if (!user["fingerprintIds"] || user["fingerprintIds"].length === 0) {
            throw new Meteor.Error(Constants.METEOR_ERRORS.REGISTRATION_INCOMPLETE, "Account registration incomplete.")
        }
        return user;
    }
}